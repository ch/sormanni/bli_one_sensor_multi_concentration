#  Copyright (c) 2020.
#  The Chancellor, Masters and Scholars of the University of Cambridge,
#  Dr Pietro Sormanni and Prof Michele Vendruscolo, all rights reserved.
#  This program is distributed with a license agreement that accompanies it
#  ("CamSol License agreement"),
#  by using this software you agree to the terms of this license
#  You should have received a copy of the license with this software.
#  IF YOU HAVEN'T RECEIVED THE LICENSE DO NOT USE THE SOFTWARE,
#  you can ask for a licence by emailing
#  Prof M. Vendruscolo (mv245@cam.ac.uk) or Dr P. Sormanni (ps589@cam.ac.uk)

import csv
import os
import sys
from collections import OrderedDict

import numpy
import xlrd

try:
    from .csv_dict import Data
except Exception:
    try:
        from csv_dict import Data
    except Exception:

        class Data(OrderedDict):
            def __init__(self, *argv, **argd):
                self.hd = {}  # header of the data class
                self.sheet_name = ""
                self.sheet_index = None  # 0,1,2,...
                self.key_column_hd_name = "keys"
                super(Data, self).__init__(*argv, **argd)  # this inits the OrderedDict
                return

            def HD(self):
                """
                just returns the hd as a list in the correct order
                """
                hd = ["?"] * (len(self.hd))
                for title in self.hd:
                    if type(self.hd[title]) is int and self.hd[title] < len(self.hd):
                        hd[self.hd[title]] = title
                return hd

            def numpymat(self, include_keys=False):
                """
                saves in self.mat the numpy matrix corresponding to the whole file (no hd)
                if include_keys keys will be first column thus ones need to manually add +1 to all hd calls
                """
                if include_keys:
                    self.mat = numpy.array([[float(k)] + self[k] for k in self])
                else:
                    self.mat = numpy.array([self[k] for k in self])
                return


def convert_to_number(
    string,
    force_float=False,
    empty_cell_value="",
    convert_to_bool={
        "True": True,
        "False": False,
        "true": True,
        "false": False,
        "TRUE": True,
        "FALSE": False,
    },
):
    """
    this function check if a string is an int or a float and it returns a tuple in the form
    converted_string,bool. Bool is True if the sting has been converted, False if the  string is still in string format.
    the function is quite slow
    """
    if convert_to_bool is not None and string in convert_to_bool:
        return convert_to_bool[string], False
    if string == "":
        return empty_cell_value, False
    if force_float:
        try:
            return float(string), True
        except ValueError:
            return string, False
    try:
        return int(string), True
    except ValueError:
        try:
            if float(string) == int(float(string)):
                return int(float(string)), True
            return float(string), True
        except ValueError:
            return string, False


def new_key_for_dict(dictionary, old_key, start_at=0):
    """
    if old_key in dictionary finds a new key that is not
    by adding 00 (if start_at is 0) and so on
    """
    ij = start_at
    while True:
        c = old_key + "%02d" % (ij)
        if c not in dictionary:
            break
        ij += 1
    return c


def list_to_dictionary(a_list):
    """
    it reads a list and return a dictionary where the keys are the list element and 
    the values are the positions of those elements in the list
    useful if I have an header in list format and I want to get it in dictionary format
    """
    dic = {}
    for i, el in enumerate(a_list):
        if el in dic:
            sys.stderr.write(
                "**WARNING** in list_to_dictionary() element %s already there." % (el)
            )
            el = new_key_for_dict(dic, el)
            sys.stderr.write(" Adding %s instead\n" % (el))
        dic[el] = i
    return dic


class Sheet_dict(OrderedDict):
    def __init__(self, *argv, **argd):
        if "fname" in argd:
            self.filename = argd["fname"]
            del argd["fname"]
        else:
            self.filename = ""  # xls file name
        self.nsheets = 0
        super(Sheet_dict, self).__init__(*argv, **argd)
        return


def read_xls_file(
    xls_inp,
    key_column=1,
    auto_convert_to_number=True,
    convert_key_to_number=False,
    force_float=False,
    empty_cell_value=numpy.nan,
    non_unicode_strings=False,
    hd={},
):
    """
    uses the xlrd module to read an excel file, it returns a sheet_dict class.
    if hd is not given as None the first row of each sheet is used as an header for that sheet.
    Note that unless force_float is True auto_convert_to_number will convert all to int for most excel files, rounding it down.
    empty_cell_value does something only if auto_convert_to_number is True
    """
    if key_column is not None:
        if key_column < 0:
            sys.stderr.write(
                "ERROR key_column must be positive (given %d) setting to 0\n"
                % (key_column)
            )
            key_column = 0
        key_column -= 1  # convert from position to index
        if key_column < 0:
            key_column = 0

    overwrite_warning = 0
    sheet_dict = Sheet_dict()
    sheet_dict.filename = xls_inp
    with xlrd.open_workbook(xls_inp) as wb:
        nsheets = wb.nsheets  # number of sheets
        sheet_dict.nsheets = nsheets
        for i in range(nsheets):
            sh = wb.sheet_by_index(i)  # or wb.sheet_by_name('name_of_the_sheet_here')
            sheet_name = str(sh.name)
            # if nsheets==1 and sheet_name=='Sheet1' :
            #    sheet_name= xls_inp.split('/')[-1].split('.')[0] # name as filename
            # if sheet_name in sheet_dict :sheet_name+=str(i)
            sheet_dict[i] = Data()
            sheet_dict[i].sheet_index = i
            sheet_dict[i].sheet_name = sheet_name
            for r in range(sh.nrows):
                vals = sh.row_values(r)
                if non_unicode_strings:
                    vals = [
                        str(a.replace("\xa0", " ").replace("\xa3", " "))
                        if type(a) is str
                        else str(a)
                        for a in vals
                    ]
                if r == 0 and hd is not None:
                    if key_column is not None:
                        sheet_dict[i].key_column_hd_name = vals.pop(
                            key_column
                        )  # also remove key from vals
                    else:
                        sheet_dict[i].key_column_hd_name = "keys"
                    if auto_convert_to_number:
                        v2 = [
                            convert_to_number(
                                v,
                                force_float=force_float,
                                empty_cell_value=empty_cell_value,
                            )[0]
                            for v in vals
                        ]
                        sheet_dict[i].hd = list_to_dictionary(
                            [
                                int(x) if type(x) is float and int(x) == x else x
                                for x in v2
                            ]
                        )
                    else:
                        sheet_dict[i].hd = list_to_dictionary(vals)
                    continue  # skip row, which was header
                if key_column is None:
                    k = r
                else:
                    k = vals.pop(key_column)  # also remove key from vals
                # print (k,type(k),convert_to_number(k,force_float=False)[0],type(convert_to_number(k,force_float=False)[0]))
                k = convert_to_number(k, force_float=False)[0]
                if not convert_key_to_number:
                    k = str(
                        k
                    )  # necessary like this as otherwise keys like '1' become '1.0'
                if auto_convert_to_number:
                    vals = [
                        convert_to_number(
                            v,
                            force_float=force_float,
                            empty_cell_value=empty_cell_value,
                        )[0]
                        for v in vals
                    ]
                if overwrite_warning < 10 and k in sheet_dict[i]:
                    sys.stderr.write(
                        "**Warn while loading %s. Overwriting entry for %s\n"
                        % (xls_inp, k)
                    )
                    overwrite_warning += 1
                    if overwrite_warning >= 10:
                        sys.stderr.write(
                            "*****   ==> Suppressing further overwrite warnings from file %s\n"
                            % (xls_inp)
                        )
                    sys.stderr.flush()
                sheet_dict[i][k] = vals
    return sheet_dict


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.stderr.write(
            "**ERROR** USAGE:\nxls_to_csv input.xls_file[s] [-outf outfoldername]\n\n"
        )
        sys.exit(1)
    inputfiles = sys.argv[1:]
    outfolder = ""
    if "-outf" in inputfiles:
        outfolder = inputfiles[inputfiles.index("-outf") + 1]
        inputfiles.remove("-outf")
        inputfiles.remove(outfolder)
        if not os.path.isdir(outfolder):
            os.mkdir(outfolder)
    elif "-outfolder" in inputfiles:
        outfolder = inputfiles[inputfiles.index("-outfolder") + 1]
        inputfiles.remove("-outfolder")
        inputfiles.remove(outfolder)
        if not os.path.isdir(outfolder):
            os.mkdir(outfolder)

    if outfolder != "" and outfolder[-1] != "/":
        outfolder += "/"

    for xls_inp in inputfiles:
        with xlrd.open_workbook(xls_inp) as wb:
            nsheets = wb.nsheets  # number of sheets
            for i in range(nsheets):
                sh = wb.sheet_by_index(
                    i
                )  # or wb.sheet_by_name('name_of_the_sheet_here')
                sheet_name = str(sh.name)
                if nsheets == 1 and sheet_name == "Sheet1":
                    sheet_name = xls_inp.split("/")[-1].split(".")[0]
                with open(outfolder + sheet_name + ".csv", "wb") as f:
                    print(
                        "Saving sheet %d [%s] from file %s to: %s.csv"
                        % (i + 1, str(sh.name), xls_inp, outfolder + sheet_name)
                    )
                    c = csv.writer(f)
                    for r in range(sh.nrows):
                        c.writerow(sh.row_values(r))
    sys.exit(0)
