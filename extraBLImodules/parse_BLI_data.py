#  Copyright (c) 2020.
#  The Chancellor, Masters and Scholars of the University of Cambridge,
#  Dr Pietro Sormanni and Prof Michele Vendruscolo, all rights reserved.
#  This program is distributed with a license agreement that accompanies it
#  ("CamSol License agreement"),
#  by using this software you agree to the terms of this license
#  You should have received a copy of the license with this software.
#  IF YOU HAVEN'T RECEIVED THE LICENSE DO NOT USE THE SOFTWARE,
#  you can ask for a licence by emailing
#  Prof M. Vendruscolo (mv245@cam.ac.uk) or Dr P. Sormanni (ps589@cam.ac.uk)

import datetime
import os
import sys
from collections import OrderedDict

import numpy
import scipy.integrate
import scipy.signal
import scipy.stats

import csv_dict
import misc
import plotter

plotter.default_parameters["fit_allow_extra_fraction"] = 0


# FITTING FUNCTIONS, last 2 actually used.
fitting_function_fixed_params = {
    "association_simple_Y0": 0,
    "dissociation_simple_Y0": 0,
    "both_fits_t_break": 1000,
    "both_fits_analyte_conc": None,
}

params = {
    "ConvertToMolar": 1e-9
}  # DEPENDS ON WHETHER YOUR INPUT CONCENTRATIONS ARE IN nM uM etc...


# for 1:1
def association_simple(t, p, y0=None):
    # p[0] is Kobs (= Ka*[analyte] *Kdiss) and p[1] is A (asymptote )
    # Y0= binding at start of association,
    if y0 is None:
        y0 = fitting_function_fixed_params["association_simple_Y0"]
    return y0 + p[1] * (1 - numpy.exp(-p[0] * (t - t[0])))


def dissociation_simple_nonfull(t, p, y0=None):  # parameter_guess=[ 1e-4 ,0]
    # p[0] is Kdiss and p[1] is A (asymptote ) assume lack of full dissociation!
    # Y0 is binding at start of dissociation
    if y0 is None:
        y0 = fitting_function_fixed_params["dissociation_simple_Y0"]
    return (y0 - p[1]) * numpy.exp(-p[0] * (t - t[0])) + p[1]


def dissociation_simple(t, p, y0=None):  # parameter_guess=[ 1e-4 ,0]
    # p[0] is Kdiss
    # Y0 is binding at start of dissociation
    if y0 is None:
        y0 = fitting_function_fixed_params["dissociation_simple_Y0"]
    return (y0) * numpy.exp(-p[0] * (t - t[0]))


# def get_Kd_from_Kobs(Kobs,Kdiss,analyte_conc) :
#    return (Kobs-Kdiss)/analyte_conc


def both_fits(t, p, analyte_conc=None, Tbreak=None, use_T0=True):
    """
    parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax']
    # assumes np arrays as input for t - association is [t<Tbreak] it is the beginning of the dissociation
    # analyte_conc is one number,  p[2]  is Rmax, p[1] is Kdiss and p[0] is Kass  
    """
    if Tbreak is None:
        Tbreak = fitting_function_fixed_params["both_fits_t_break"]
    if analyte_conc is None:
        analyte_conc = fitting_function_fixed_params["both_fits_analyte_conc"]
    if analyte_conc is None:
        print("**WARNING** analyte_conc is None, setting to 1 - Fitting OBSERVED Kon")
        analyte_conc = 1  # Will fit the observed Kon in this way.
    elif hasattr(analyte_conc, "__len__"):
        analyte_conc = analyte_conc[0]
    T0 = 0
    # print('t[:3]=',list(t)[:3],'p=',p,'conc=',analyte_conc)
    if use_T0:
        T0 = t[0]
        if T0 < 0:
            print("!!! T0=", T0, min(t), "maybe fit_allow_extra_fraction?")
            print("t[:20]=", list(t)[:20], "p=", p)
            # t+=T0
            # T0=0
    t = numpy.array(t)  # start from zero
    Yass = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (t[t <= Tbreak] - T0)))
    )  #
    Y0 = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (Tbreak - T0)))
    )  # last time of Yass to set beginning of Ydiss
    Ydiss = Y0 * numpy.exp(-p[1] * (t[t > Tbreak] - Tbreak))
    # print Yass[-1],Y0,Ydiss[0]
    return numpy.hstack((Yass, Ydiss))


def both_fits_independent(t, p, analyte_conc=None, Tbreak=None):
    """
    parameters_name=['kass (1/Ms)','kdiss(Ass.phase) (1/s)','Rmax','kdiss(Diss.phase) (1/s)']
    # like both fits but has one kdiss for association phase and one independent for dissociation p[3]
    assumes np arrays as input for t - association is [t<Tbreak] it is the beginning of the dissociation
    # analyte_conc is one number,  p[2]  is Rmax, p[1] is Kdiss and p[0] is Kass  
    """
    if Tbreak is None:
        Tbreak = fitting_function_fixed_params["both_fits_t_break"]
    if analyte_conc is None:
        analyte_conc = fitting_function_fixed_params["both_fits_analyte_conc"]
    if analyte_conc is None:
        print("**WARNING** analyte_conc is None - Fitting OBSERVED Kon")
        analyte_conc = 1  # Will fit the observed Kon in this way.
    elif hasattr(analyte_conc, "__len__"):
        analyte_conc = analyte_conc[0]
    T0 = t[0]
    if T0 < 0:
        print("!!! T0=", T0, min(t), "maybe fit_allow_extra_fraction?")
        # t+=T0
        # T0=0
    t = numpy.array(t)  # start from zero
    Yass = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (t[t <= Tbreak] - T0)))
    )  #
    Y0 = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (Tbreak - T0)))
    )  # last time of Yass to set beginning of Ydiss
    Ydiss = Y0 * numpy.exp(-p[3] * (t[t > Tbreak] - Tbreak))
    # print Yass[-1],Y0,Ydiss[0]
    return numpy.hstack((Yass, Ydiss))


def both_fits_nonfull_dissociation(t, p, analyte_conc=None, Tbreak=None):
    """
    parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax','dissociation y[t->inf]' ]
    # parameter_guess=[72,1e-4,0.2, 0]
    # assumes np arrays as input for t - association is [t<Tbreak] it is the beginning of the dissociation
    # analyte_conc is one number,  p[2]  is Rmax, p[1] is Kdiss and p[0] is Kass p[3] is the lower plateau of the dissociation
    """
    if Tbreak is None:
        Tbreak = fitting_function_fixed_params["both_fits_t_break"]
    if analyte_conc is None:
        analyte_conc = fitting_function_fixed_params["both_fits_analyte_conc"]
    if analyte_conc is None:
        print("**WARNING** analyte_conc is None - Fitting OBSERVED Kon")
        analyte_conc = 1  # Will fit the observed Kon in this way.
    elif hasattr(analyte_conc, "__len__"):
        analyte_conc = analyte_conc[0]
    T0 = t[0]
    if T0 < 0:
        print("!!! **WARNING*** T0=", T0, min(t), "maybe fit_allow_extra_fraction?")
    t = numpy.array(t)  # start from zero
    Yass = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (t[t < Tbreak] - T0)))
    )
    Y0 = (
        p[2]
        * (1 / (1 + p[1] / (p[0] * analyte_conc)))
        * (1 - numpy.exp(-(p[0] * analyte_conc + p[1]) * (Tbreak - T0)))
    )
    Ydiss = (Y0 - p[3]) * numpy.exp(-p[1] * (t[t >= Tbreak] - Tbreak)) + p[3]
    # print Yass[-1],Y0,Ydiss[0]
    return numpy.hstack((Yass, Ydiss))


def get_data_for_concentration_series_fit_from_phase_dict(
    fit_series_phase_dict, window_length=111, polyorder=2, sm_jump=20
):
    """
    return ydata,times, ydata_sm,times_sm, tbreaks
    sm_jump determines how many points will be returned in the smoothed profiles (every sm_jump points)
    """
    ydata = None
    times = None
    ydata_sm = None
    times_sm = None
    tbreaks = []
    for ph in fit_series_phase_dict:
        if ph == "baseline":
            continue
        t = fit_series_phase_dict[ph][0]
        tbreaks += [t[-1]]
        if times is None:
            times = t
            times_sm = t[sm_jump:-sm_jump][::sm_jump]
        else:
            times = numpy.hstack((times, t))
            times_sm = numpy.hstack((times_sm, t[sm_jump:-sm_jump][::sm_jump]))
        if ydata is None:
            ydata = fit_series_phase_dict[ph][1]
            ydata_sm = misc.smooth_profile(
                fit_series_phase_dict[ph][1],
                use_savgol_filter=(window_length, polyorder),
            )[sm_jump:-sm_jump][::sm_jump]
        else:
            ydata = numpy.hstack((ydata, fit_series_phase_dict[ph][1]))
            ydata_sm = numpy.hstack(
                (
                    ydata_sm,
                    misc.smooth_profile(
                        fit_series_phase_dict[ph][1],
                        use_savgol_filter=(window_length, polyorder),
                    )[sm_jump:-sm_jump][::sm_jump],
                )
            )
    return ydata, times, ydata_sm, times_sm, tbreaks




def one_sensor_multiconcentration_fit(phase_dict, analyte_concs_in_nM, Kass_Kdiss_guesses=[30000,0.1 ], Rmax_guess=None, label='',fit_model='STANDARD' ,return_source_data=False ,all_fit_data=None ,do_fit_on_smoothed_data=False,fit_window_length=111,fit_polyorder=2,sm_jump=20,smoothed_color = 'white', Remove_last_N_point=None, vgrid=True,color=None,fit_label=None,fit_linecolor=None,fit_linewidth=2,fit_linestyle='-', hline=[0],legend_size=18,title=None,figure=None,figure_size=None,fit_CI=False,show=True,**profile_kwargs) :
    '''
    # CHOOSE RIGHT FITTING MODEL coded_model_options = ['STANDARD','Yinf','YinfLin','LOCAL_Rmax','None'] ('None' will plot data but won't fit anyting)
    # MAY ALSO NEED TO ADJUST PARAMETERS GUESS Kass_Kdiss_Rmax_guesses (extra params for fitted models are guessed automatically)
    return all_fit_data, fit_data, figure
    if return_source_data :
        return all_fit_data, fit_data, figure, source_data
        where source_data is a csv_dict class containing two columns (time and ys) of the plotted data
    all_fit_data can be given as csv_dict Data class (as returned by previous call of this function),
        if so fit results are added to this, else a new class is created.
    label is used in plot but also in all_fit_data
    if do_fit_on_smoothed_data is False fit_window_length=111,fit_polyorder=2,sm_jump=20 do nothing
        set smoothed_color to 'none' if you don't want to display the smoothed dots
    Remove_last_N_point can be used to shorten the dissociation phase
    vgrid=True will use tbreaks between phases as vertical grid
    if Rmax_guess is None it will be set to the max of the Ydata as found in phase_dict
    '''
    analyte_concs_M = 1e-9*numpy.array(analyte_concs_in_nM) # convert to Molar and numpy array
    if Remove_last_N_point==0 :
        Remove_last_N_point=None
    elif Remove_last_N_point is not None and Remove_last_N_point>0 :
        Remove_last_N_point*=-1
    #all_fit_results={}
    if all_fit_data is None :
        all_fit_data=csv_dict.Data()
    ydata,times, ydata_sm,times_sm, tbreaks = get_data_for_concentration_series_fit_from_phase_dict(phase_dict, window_length=fit_window_length, polyorder=fit_polyorder, sm_jump=sm_jump)
    ydata=ydata[:Remove_last_N_point]
    times=times[:Remove_last_N_point]
    
    # define fitting model to use
    coded_model_options = ['STANDARD','Yinf','Partial','YinfLin','YinfFrac','PartialLin','LOCAL_Rmax','None']
    if fit_model.upper() not in [a.upper() for a in coded_model_options ] :
        raise Exception("one_sensor_multiconcentration_fit() given fit_model='%s' but available coded options are %s"%(fit_model,coded_model_options))
    if Rmax_guess is None :
        Rmax_guess = 1.2*max(ydata)
    parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax' ]    
    if fit_model.upper() == 'STANDARD' :
        # STANDARD with GLOBAL Rmax (note there are many different Rass and Rdiss one could choose from, including attempts at heterogeneous fits with Rass_het):
        fit_fun = lambda t,p : Rfit(t,p, analyte_conc=analyte_concs_M,Tbreak=tbreaks, association_function=Rass,dissociation_function=Rdiss)
        fit_tup = (fit_fun, Kass_Kdiss_guesses+[Rmax_guess])
    elif fit_model.upper() in [ 'YINF' , 'PARTIAL' ] :
        #  GLOBAL Rmax but DISSOCIATION DOES NOT GO TO ZERO (parameter guess becomes more important)
        #OLD: fit_fun = lambda t,p : Rfit(t,p[:3] , local_params=p[3:], analyte_conc=analyte_concs_M,Tbreak=tbreaks, association_function=Rass,dissociation_function=Rdiss)
        fit_fun = lambda t,p : Rfit_partialDissociation_multiConcentration(t,p[0],p[1],Rmax_guess_or_list=p[2],Ytinf_guess_or_list=p[3:],analyte_conc=analyte_concs_M,Tbreak=tbreaks,constrain_multiple_Ytinf_to_maxPreviousOne=True)
        fit_tup = (fit_fun, Kass_Kdiss_guesses+[Rmax_guess]+ [0.001*j for j,x in enumerate(analyte_concs_M)]) # add extra parameter, in which case Yinf instead of 0 is fitted
        parameters_name=parameters_name[:]+['Yinf'+str(c) for c in  analyte_concs_in_nM ]
    elif fit_model.upper() in [ 'YINFFRAC' , 'YINF_FRAC' ] :
        #  GLOBAL Rmax but DISSOCIATION DOES NOT GO TO ZERO (parameter guess becomes more important), only one Yinf parameter for all concentration, corresponding to the fraction of the max signal at end of association that it can decrease to (1= no decrease, 0= full decrease to 0)
        fit_fun = lambda t,p : Rfit_partialDissociation_multiConcentration(t,p[0],p[1],Rmax_guess_or_list=p[2],Ytinf_guess_or_list=p[3],Ytinf_guess_is_fraction_of_associationEnd=True ,analyte_conc=analyte_concs_M,Tbreak=tbreaks,constrain_multiple_Ytinf_to_maxPreviousOne=True)
        fit_tup = (fit_fun, Kass_Kdiss_guesses+[Rmax_guess]+ [0.33]) # add extra parameter, in which case Yinf instead of 0 is fitted
        parameters_name=parameters_name[:]+['YinfFraction_of_associationEnd' ]
    elif fit_model.upper() in [ 'YINFLIN','PARTIALLIN'  ] :
        #  GLOBAL Rmax but DISSOCIATION DOES NOT GO TO ZERO (parameter guess becomes more important)
        #OLD: fit_fun = lambda t,p : Rfit(t,p[:3] , local_params=p[3:], analyte_conc=analyte_concs_M,Tbreak=tbreaks, association_function=Rass,dissociation_function=Rdiss)
        fit_fun = lambda t,p : Rfit_partialDissociation_multiConcentration_LinearYinf(t,p, analyte_conc=analyte_concs_M,Tbreak=tbreaks,constrain_multiple_Ytinf_to_maxPreviousOne=True)
        fit_tup = (fit_fun, Kass_Kdiss_guesses+ [0.1,0.001] +[Rmax_guess]) # the extra parameters in the middle are m_Ytinf, q_Ytinf (slope and intercet)
        parameters_name=parameters_name[:2]+['Yinf_slope','Yinf_intercept' ]+parameters_name[2:]
    elif fit_model.upper() == 'LOCAL_RMAX' :
        # Using LOCAL Rmax:
        fit_fun = lambda t,p : Rfit(t,p[:2] , local_params=p[2:], analyte_conc=analyte_concs_M,Tbreak=tbreaks, association_function=Rass,dissociation_function=Rdiss)
        fit_tup = (fit_fun, Kass_Kdiss_guesses+[Rmax_guess for x in analyte_concs_M] )
        parameters_name=parameters_name[:2]+['Rmax'+str(c) for c in  analyte_concs_in_nM ]
    elif fit_model.upper() == 'NONE' :
        print("Fit model set to None, will not fit")
        fit_tup=None
    
    # BIVALENT MODEL (just pasted here, should look at notebook in /Users/pietro/Dropbox/CAM/Structured_epitopes/Antibodies Structured Epitopes/Data/Ross_BLI_data )
    #fit_fun = lambda t,p : Rfit(t, p, analyte_conc=analyte_concs_M, Y0=[0,0], T0=None, Tbreak=tbreaks, association_function=Rass_biv, dissociation_function=Rdiss_biv ,full_return=False)
    
    fit_results={}
    if vgrid :
        vgrid=tbreaks
    data_label=label
    if fit_label is not None :
        data_label=None
    if do_fit_on_smoothed_data :
        print("do_fit_on_smoothed_data for %s left with Npoints ydata_sm: %d times_sm: %d (of %d %d)"%(label,len(ydata_sm),len(times_sm),len(ydata),len(times)))
        figure=plotter.profile(ydata, times, label=data_label,fit_label=fit_label, vgrid=vgrid,hline=hline,legend_size=legend_size,color=color,figure_size=figsize,figure=figure,show=show)
        figure=plotter.profile(ydata_sm, times_sm, ls='',marker='.',markersize=2,markerfacecolor=smoothed_color,markeredgecolor=smoothed_color,alpha=0.6,figure=figure, linfit=fit_tup,fit_allow_extra_fraction=0,fit_linecolor=fit_linecolor,fit_linestyle=fit_linestyle,fit_linewidth=fit_linewidth,fit_results=fit_results,title=title,calculate_fit_CI=fit_CI,plot_fit_CI=fit_CI,show=show,**profile_kwargs)
    else :
        figure=plotter.profile(ydata, times, label=data_label,fit_label=fit_label, linfit=fit_tup,fit_allow_extra_fraction=0,vgrid=vgrid,hline=[0],color=color,fit_linecolor=fit_linecolor,fit_linestyle=fit_linestyle,fit_linewidth=fit_linewidth,fit_results=fit_results,calculate_fit_CI=fit_CI,plot_fit_CI=fit_CI,legend_size=legend_size,figure_size=figure_size,title=title,figure=figure,show=show,**profile_kwargs)
    fit_data={}
    if fit_tup is not None :
        params=fit_results['parameters_fitted'][:]
        #yf=fit_fun(times,[params[0]*4,params[1]/4, params[2]])
        #figure=plotter.profile(yf,times,label='p=%s kd=%g nM'%(str(params),1e9*params[1]/params[0]),color='DarkBlue',legend_size=10,figure=figure)
        # print fitted parameters and get KD
        print("\n%s fitting:"%(label))
        print('tbreaks=',tbreaks,len(tbreaks))
        extra_str=''
        if fit_label is not None and data_label is not None : extra_str=' '+fit_label+' '+data_label
        elif fit_label is None and data_label is not None : extra_str=' '+data_label
        elif fit_label is not None and data_label is None : extra_str=' '+fit_label
        fit_data=plotter.print_fit_results(fit_results,parameters_name=parameters_name,return_data_class=True,extra_str=extra_str)
        Kd,Kd_err = misc.propagate_error(lambda x,y : x/y, [fit_results['parameters_fitted'][1], fit_results['parameters_fitted'][0]] , [fit_results['parameters_standard_errors'][1], fit_results['parameters_standard_errors'][0]])
        sys.stdout.write('KD= %g +/- %g M\n***********************************************************\n\n' % (Kd,Kd_err))
        fit_data['KD (M)']=[Kd,Kd_err]
        #all_fit_results[label]=fit_data
        print('')
        # save in big data class
        if all_fit_data.hd in [{},None] :
            all_fit_data.hd=csv_dict.list_to_dictionary(['Variant','Parameter']+fit_data.HD())
        for k in fit_data :
            all_fit_data[label+'_'+k]= [label, k ]+fit_data[k][:]
    print("DEB fit_results.keys()",fit_results.keys())
    if return_source_data :
        source_data=csv_dict.Data()
        L=len(ydata)
        if fit_results is not None and 'xs_fitted' in fit_results and len(fit_results['xs_fitted'])>L :
            L=len(fit_results['xs_fitted'])
        for k in range(L) : source_data[k]=[] 
        source_data.add_column(times, label+' Time (s)')
        source_data.add_column(ydata, label+' Binding (nm)')
        if fit_results is not None and 'xs_fitted' in fit_results :
            source_data.add_column(fit_results['xs_fitted'], label+' FIT Time (s)')
            source_data.add_column(fit_results['ys_fitted'], label+' FIT Binding (nm)')
        return all_fit_data, fit_data, figure,source_data
    return all_fit_data, fit_data, figure


def concentration_series_fit(t, p, analyte_concs=None, t_breaks=None):
    """
    CAREFUL works only with fit_allow_extra_fraction=0
    t_breaks should be all breaks between association/dissociation and dissociation/next association
    parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax' ]
    # parameter_guess=[72,1e-4,0.2, 0]
    # assumes np arrays as input for t - association is [t<Tbreak] it is the beginning of the dissociation
    # analyte_conc is one number,  p[2]  is Rmax, p[1] is Kdiss and p[0] is Kass p[3] is the lower plateau of the dissociation
    """
    if t_breaks is None:
        raise Exception(
            "\n**cannot do concentration_series_fit with t_breaks set to None**\nUse fit_fun=lambda t,p : concentration_series_fit(t, p, analyte_concs=analyte_concs,t_breaks=t_breaks)\n"
        )
    if analyte_concs is None:
        raise Exception(
            "\n**cannot do concentration_series_fit with t_breaks set to None**\nUse fit_fun=lambda t,p : concentration_series_fit(t, p, analyte_concs=analyte_concs,t_breaks=t_breaks)\n"
        )
    t_breaks = list(t_breaks)
    if t_breaks[-1] < t[-1]:
        t_breaks += [t[-1]]
    if t_breaks[0] > t[0]:
        t_breaks = [t[0]] + t_breaks
    if len(t_breaks) > 2 * len(analyte_concs) + 1:
        sys.stderr.write(
            "**ERROR** in concentration_series_fit() len(t_breaks)>2*len(analyte_concs)+1 len(t_breaks)=%d len(analyte_concs)=%d - have you set fit_allow_extra_fraction=0?\nt_breaks=%s max_t=%g, min_t=%g t[0]=%g t[-1]=%g"
            % (
                len(t_breaks),
                len(analyte_concs),
                str(t_breaks),
                max(t),
                min(t),
                t[0],
                t[-1],
            )
        )
    Ytinf = 0
    Y0 = 0
    kon, koff, Rmax = p
    all_ys = None
    t = numpy.array(t)  # start from zero
    # print('DEB',len(t_breaks),t_breaks)
    # Yass = Rmax * (1/(1 + koff/(kon*analyte_conc))) * (1 - numpy.exp(-(kon*analyte_conc + koff)*(t[t<=Tbreak]-T0) )) #
    # Y0 = Rmax * (1/(1 + koff/(p[0]*analyte_conc))) * (1 - numpy.exp(-(p[0]*analyte_conc + koff)*(Tbreak-T0) )) # last time of Yass to set beginning of Ydiss
    # Ydiss= Y0*numpy.exp(-p[1]*(t[t>Tbreak]-Tbreak))
    for j, t0 in enumerate(t_breaks[:-1]):
        if j % 2 == 0:
            tass = t[((t >= t0) & (t < t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tass[0] > t0:
                tass = numpy.insert(tass, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (
                tass[-1] < t_breaks[j + 1]
            ):  # important to add to get correct boundary conditions for later
                tass = numpy.append(tass, [t_breaks[j + 1]])
                pop_one = True
            # get solution
            Yass = (
                Y0 - Rmax / (1 + (koff / (kon * analyte_concs[j // 2])))
            ) * numpy.exp(
                -(kon * analyte_concs[j // 2] + koff) * (tass - t0)
            ) + Rmax / (
                1 + (koff / (kon * analyte_concs[j // 2]))
            )

            # retrieve initial conditions for next phase
            Y0 = Yass[
                -1
            ]  # initial condition will be end of association phase estimated exactly at Tbreak
            if rem_first:
                Yass = Yass[1:]
            if pop_one:
                Yass = Yass[:-1]
            if all_ys is None:
                all_ys = Yass
            else:
                all_ys = numpy.hstack((all_ys, Yass))
        else:
            tdiss = t[((t >= t0) & (t < t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tdiss[0] > t0:
                tdiss = numpy.insert(tdiss, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (
                tdiss[-1] < t_breaks[j + 1]
            ):  # important to add to get correct boundary conditions for later
                tdiss = numpy.append(tdiss, [t_breaks[j + 1]])
                pop_one = True
            # get solution
            Ydiss = (Y0 - Ytinf) * numpy.exp(-koff * (tdiss - t0)) + Ytinf
            # retrieve initial conditions for next phase
            Y0 = Ydiss[
                -1
            ]  # initial condition will be end of association phase estimated exactly at Tbreak
            if rem_first:
                Ydiss = Ydiss[1:]
            if pop_one:
                Ydiss = Ydiss[:-1]
            all_ys = numpy.hstack((all_ys, Ydiss))
    if len(all_ys) == len(t) - 1:
        all_ys = numpy.hstack((all_ys, Y0))  # add last element
    return all_ys


### EQUATIONS FROM SPR EVILFIT PAPER ####
def Seq(conc, KA, Smax):  # signal equivalent
    return Smax / (1 + 1.0 / (KA * conc))


def Sass(times, params, T0=0, Y0=0):
    conc, KA, Kon, Smax = params  # unpack parameters
    return (Y0 - Seq(conc, KA, Smax)) * numpy.exp(
        -(Kon * conc + 1 / KA) * (times - T0)
    ) + Seq(conc, KA, Smax)


def Sdiss(times, params, Y_Tbreak, Tbreak, Ytinf=0):
    conc, KA, Kon, Smax = params  # unpack parameters
    return (Y_Tbreak - Ytinf) * numpy.exp(-(Kon / KA) * (times - Tbreak)) + Ytinf


def S(times, KA, Kon, Smax, b0=0, b1=0, conc=None, Tbreak=100, T0=0):
    if conc is None:
        raise Exception("def S fitting function must give analyte conc\n")
    Ybaseline = numpy.array([b0 for t in times[times < T0]])
    Yass = (
        Sass(times[((times >= T0) & (times < Tbreak))], conc, KA, Kon, Smax, T0=T0) + b1
    )
    Y_Tbreak = Sass(Tbreak, conc, KA, Kon, Smax, T0=T0) + b1
    Ydiss = Sdiss(
        times[times >= Tbreak], KA, Kon, Y_Tbreak=Y_Tbreak, Tbreak=Tbreak, Ytinf=b0
    )
    return numpy.hstack((Ybaseline, Yass, Ydiss))


#### ADAPTATION of above for fitting with kass and koff ####
def ReqKD(conc, KD, Rmax):
    return Rmax / (1.0 + KD / (conc))


def Req(conc, kass, kdiss, Rmax):
    return Rmax / (1.0 + kdiss / (kass * conc))


def Rass(t, params, Y0=0, T0=0):
    # unpack parameters
    #print("DEB: len(params)",len(params))
    if len(params)==5 :
        conc, kass, kdiss, Rmax, Ytinf = params # fit also Ytinf (here does nothing as this is association)
    else :
        conc, kass, kdiss, Rmax = params  
    if kass <= 0 or kdiss < 0 or Rmax <= 0:  # sentinel
        return 1e7 * numpy.ones(t.shape)
    return (Y0 - Req(conc, kass, kdiss, Rmax)) * numpy.exp( -(kass * conc + kdiss) * (t - T0)) + Req(conc, kass, kdiss, Rmax)


def Rdiss(t, params, Ytbreak=1, Tbreak=0, Ytinf=0):
    # unpack parameters
    if len(params)==5 :
        conc, kass, kdiss, Rmax, Ytinf = params # fit also Ytinf
        if kdiss < 0:  # sentinel
            return 1e7 * numpy.ones(t.shape)
        if Ytinf>Ytbreak :  # sentinel, makes it very sensitive to parameter guess
            return 1e3* numpy.ones(t.shape) #*(Ytinf-Ytbreak)*numpy.ones(t.shape) + (Ytbreak - Ytinf) * numpy.exp(-(kdiss) * (t - Tbreak)) + Ytinf
        if Ytinf<0 : # sentinel
            return  1e3 * numpy.ones(t.shape)
    else :
        conc, kass, kdiss, Rmax = params  
        if kdiss < 0:  # sentinel
            return 1e7 * numpy.ones(t.shape)
    return (Ytbreak - Ytinf) * numpy.exp(-(kdiss) * (t - Tbreak)) + Ytinf


def Rfit_partialDissociation_multiConcentration_LinearYinf(times,params,analyte_conc=None,Tbreak=None,Y0=0,T0=None,full_return=False, constrain_multiple_Ytinf_to_maxPreviousOne=True):
    '''
    will use Rfit_partialDissociation_multiConcentration
    here params MUST be: 
        kass, kdiss, m_Ytinf, q_Ytinf, Rmax_guess_or_list
     where Rmax_guess_or_list can be a single float, or multiple folats (not a list, just give a longer params list) 
     one per analyte_conc (of course this only makes sense if you have multiple analyte concentrations, in which case Rmax will be fitted locally to each concentration rather than globally)
    m_Ytinf, q_Ytinf only make sense for multiple analyte concentrations, 
      and it's a way in-between having a local Ytinf in a partial dissociation model. 
      The problem for having a local Ytinf in a partial dissociation model where several concentrations
      are probed by the same sensor is that the fit becomes quite unstable especially if 
      one sets constrain_multiple_Ytinf_to_maxPreviousOne to True (which forces Ytinf of lower analyte concentrations to be <= than those of higher analyte concentrations)
    In this setup the Ytinf of each analyte concentration will be linearly dependent to those of the other
     which reduces the number of free parameters to two (the slope m_Ytinf) and the intercept q_Ytinf
     when the slope is zero you effectively have a single (global) Ytinf = q_Ytinf
    '''
    kass, kdiss, m_Ytinf, q_Ytinf = params[:4]
    Rmax_guess_or_list = params[4:]
    if len(Rmax_guess_or_list)<=1 : # only one, fit globally! (if zero will throw exception)
        Rmax_guess_or_list=Rmax_guess_or_list[0]
    if not hasattr(analyte_conc,'__len__') or len(analyte_conc)<=1 :
        sys.stderr.write("***BIG WARNING*** in Rfit_partialDissociation_multiConcentration_LinearYinf the input analyte_conc=%s but this type of fit really only makes sense if there are multiple increasing analyte concentrations in the experiment!\n"%(str(analyte_conc)))
    # define Ytinf linearly with concentration:
    Ytinf_guess_or_list=[ c*m_Ytinf + q_Ytinf for c in analyte_conc]
    #print(  'DEB: Ytinf_guess_or_list=',Ytinf_guess_or_list,'analyte_conc=',analyte_conc)
    return Rfit_partialDissociation_multiConcentration(times,kass,kdiss,Rmax_guess_or_list,Ytinf_guess_or_list,analyte_conc=analyte_conc,Tbreak=Tbreak,Y0=Y0,T0=T0,full_return=full_return,constrain_multiple_Ytinf_to_maxPreviousOne=constrain_multiple_Ytinf_to_maxPreviousOne)








def Rfit_partialDissociation_multiConcentration(
    times,
    kass,
    kdiss,
    Rmax_guess_or_list,
    Ytinf_guess_or_list,   
    analyte_conc=None,
    Ytinf_guess_is_fraction_of_associationEnd=False,
    Tbreak=None,
    Y0=0,
    T0=None,
    full_return=False,
    constrain_multiple_Ytinf_to_maxPreviousOne=True,
):
    '''
    as from function name. 
    Technically should be obtainable by the generally written Rfit but 
     for debug purposes I tried this more explicit version (as Rfit can do multiple models depending on input)
    constrain_multiple_Ytinf_to_maxPreviousOne will force Ytinf of lower analyte concentrations to be <= than those of higher analyte concentrations
    '''
    if Tbreak is None:
        Exception(
            "\n**cannot do Rfit with Tbreak set to None**\nUse fit_fun=lambda t,p : Rfit(t, p, analyte_conc=analyte_conc,Tbreak=Tbreak)\n"
        )
    if analyte_conc is None:
        raise Exception(
            "\n**cannot do Rfit with analyte_conc set to None**\nUse fit_fun=lambda t,p : Rfit(t, p, analyte_conc=analyte_conc,Tbreak=Tbreak)\n"
        )
    
    if T0 is None:
        T0 = times[0]  # default option
    Ybaseline = numpy.array([Y0 for t in times[times < T0]]) # fully optional, in case there are any point this will be returned too

    if not hasattr(analyte_conc, "__len__"):
        analyte_concs = [analyte_conc]
    else:
        analyte_concs = list(analyte_conc)

    if hasattr(Rmax_guess_or_list, '__len__') and len(Rmax_guess_or_list)!=len(analyte_conc):
        raise Exception("\n** given Rmax_guess_or_list as list, but its length is different from that of analyte_conc (given %d and %d)\n"%(len(Rmax_guess_or_list),len(analyte_conc)))
    if hasattr(Ytinf_guess_or_list, '__len__') and Ytinf_guess_is_fraction_of_associationEnd :
        raise Exception("\n** given Ytinf_guess_or_list as list, but also Ytinf_guess_is_fraction_of_associationEnd=True - uncompatbile inputs give Ytinf_guess_or_list as number in 0-1 with Ytinf_guess_is_fraction_of_associationEnd=True\n")
    if hasattr(Ytinf_guess_or_list, '__len__') and len(Ytinf_guess_or_list)!=len(analyte_conc):
        raise Exception("\n** given Ytinf_guess_or_list as list, but its length is different from that of analyte_conc (given %d and %d)\n"%(len(Ytinf_guess_or_list),len(analyte_conc)))

    if Ytinf_guess_or_list is None :
        Ytinf_guess_or_list=0 # revert to standard fit model
    
    if not hasattr(Tbreak, "__len__"):
        t_breaks = [T0, Tbreak]
    else:
        t_breaks = list(Tbreak)
    
    if t_breaks[-1] < times[-1]:
        t_breaks += [times[-1]]
    if t_breaks[0] > T0:
        t_breaks = [T0] + t_breaks
    if len(t_breaks) > 2 * len(analyte_concs) + 1:
        sys.stderr.write(
            "**ERROR** in concentration_series_fit() len(t_breaks)>2*len(analyte_concs)+1 len(t_breaks)=%d len(analyte_concs)=%d - have you set fit_allow_extra_fraction=0?\n"
            % (len(t_breaks), len(analyte_concs))
        )

    # FORCE some boundaries
    if any(numpy.array([kass,kdiss]) < 0): # sentinel
        return 1e5 * numpy.ones(times.shape)
    
    nextY0 = Y0
    Yass=None
    base = 0 # starting point of association phase (first should be zero but then one need to adjust in a multi-concentration single-sensor fit)
    vals = None # resulting Ys (fitted or modelled)
    Previous_Ytinf=0 # boundaries on fit imposed (only if constrain_multiple_Ytinf_to_maxPreviousOne)
    for j, t0 in enumerate(t_breaks[:-1]):
        if hasattr(Rmax_guess_or_list, '__len__') :
            Rmax = Rmax_guess_or_list[j//2]
        else :
            Rmax = Rmax_guess_or_list
        if Rmax < 0 :
            return 1e5 * numpy.ones(times.shape)
        if hasattr(Ytinf_guess_or_list, '__len__') :
            Ytinf = Ytinf_guess_or_list[j//2]
            if constrain_multiple_Ytinf_to_maxPreviousOne and Ytinf < 0.99*Previous_Ytinf :
                return 1e5 * numpy.ones(times.shape)
        else :
            Ytinf = Ytinf_guess_or_list
        
        #print("deb Ytinf",Ytinf)
        if Ytinf< 0 : # sentinel
            return 1e5 * numpy.ones(times.shape)
        elif Ytinf_guess_is_fraction_of_associationEnd and Ytinf>1 :# sentinel
            return 1e5 * numpy.ones(times.shape)
        
        Previous_Ytinf = Ytinf # used only if guess/actual numbers given as list
        analyte_conc = analyte_concs[j//2]
        
        if (t0 == t_breaks[j + 1]):  # maybe there are multiple associations but not a dissociation in between
            continue
        if j % 2 == 0:  # this is an association phase
            tass = times[((times >= t0) & (times <= t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tass[0] > t0:
                tass = numpy.insert(tass, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (tass[-1] < t_breaks[j + 1]):  # important to add to get correct boundary conditions for later
                tass = numpy.append(tass, [t_breaks[j + 1]])
                pop_one = True
            #Yass = base + association_function(tass, params, Y0=nextY0, T0=t0)
            Yass = base + (nextY0 - Req(analyte_conc, kass, kdiss, Rmax)) * numpy.exp( -(kass * analyte_conc + kdiss) * (tass - t0)) + Req(analyte_conc, kass, kdiss, Rmax)
            # retrive correct initial conditions for next phase
            nextY0 = Yass[-1]
            if rem_first:  # adjust back so equal number of points as in input times
                Yass = Yass[1:]
            if pop_one:
                Yass = Yass[:-1]
            # add to fitted/theoretical trace
            if len(Yass.shape) > 1:  # probably a bivalent binding
                if vals is None:
                    if len(Ybaseline) > 0:
                        vals = numpy.vstack((Ybaseline, Yass))
                    else:
                        vals = Yass
                else:
                    vals = numpy.vstack((vals, Yass))
            elif vals is None:
                vals = numpy.hstack((Ybaseline, Yass))  # here if Ybaseline is empty no problem
            else:
                vals = numpy.hstack((vals, Yass))

        else:  # dissociation phase
            if Ytinf_guess_is_fraction_of_associationEnd : # overwrite now that we have calcuated nextY0
                Ytinf = Ytinf_guess_or_list*nextY0
            if Yass is not None and Ytinf >= nextY0 : # max(Yass) : # sentinel
                return 1e5 * numpy.ones(times.shape)
            # select right times for dissociation phase
            tdiss = times[((times > t0) & (times < t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tdiss[0] > t0:
                tdiss = numpy.insert(tdiss, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (tdiss[-1] < t_breaks[j + 1]):  # important to add to get correct boundary conditions for later
                tdiss = numpy.append(tdiss, [t_breaks[j + 1]])
                pop_one = True
            #Ydiss = base + dissociation_function(tdiss, params, Ytbreak=nextY0, Tbreak=t0, Ytinf=Ytinf)
            Ydiss = base + (nextY0 - Ytinf) * numpy.exp(-(kdiss) * (tdiss - t0)) + Ytinf
            # retrive correct initial conditions for next phase
            nextY0 = Ydiss[-1]
            if rem_first:
                Ydiss = Ydiss[1:]
            if pop_one:
                Ydiss = Ydiss[:-1]
            # add to fitted/theoretical trace
            if len(Ydiss.shape) > 1:  # probably a bivalent binding
                if vals is None:
                    if len(Ybaseline) > 0:
                        vals = numpy.vstack((Ybaseline, Ydiss))
                    else:
                        vals = Ydiss
                else:
                    vals = numpy.vstack((vals, Ydiss))
            elif vals is None:
                vals = numpy.hstack( (Ybaseline, Ydiss) )  # here if Ybaseline is empty no problem
            else:
                vals = numpy.hstack((vals, Ydiss))

    if ( len(vals) == len(times) - 1 ):  # we missed the last tbreak as we removed it but first we excluded it with <
        if len(vals.shape) > 1:
            vals = numpy.vstack((vals, nextY0))
        else:
            vals = numpy.append(vals, nextY0)  # but we saved it anyways
    if len(vals.shape) > 1:  # probably a bivalent binding
        vals = vals.T
        R = vals.sum(axis=0) # summed Response for bivalent binding
        if full_return:
            return R, vals
        return R
    return vals










def Rfit(
    times,
    parameters,
    analyte_conc=None,
    Tbreak=None,
    Y0=0,
    T0=None,
    Ytinf=0,
    local_params=None,
    association_function=Rass,
    dissociation_function=Rdiss,
    full_return=False,
):
    """
    # params are those of the specified association_function and dissociation_function
    # for normal one-to-one binding params=[conc , kass, kdiss, Rmax] and functions are respectively Rass and Rdiss
    Y0 must be e.g. [0,0] for bivalent binding models as it represents the initial conditions.
      full_return is only meaningful for bivalent or multivalent models, as it returns R, [R1,R2]
    if you have multiple associations but not a dissociation in-between you should give duplicated Tbreak
        (so essentially give a 0-length dissociation phase between each association phase)
    local_params can be given as a list, containing the local parameters per concentration in flattened way
      the number per concentration will be estimated from the length of this list and the number of analyte_conc given 
      e.g. for 2 local parameters and 3 concentrations give something like
        local_params=[ loc_p1_conc1, loc_p2_conc1,  loc_p1_conc2, loc_p2_conc2,  loc_p1_conc3, loc_p2_conc3]
    """
    if Tbreak is None:
        Exception(
            "\n**cannot do Rfit with Tbreak set to None**\nUse fit_fun=lambda t,p : Rfit(t, p, analyte_conc=analyte_conc,Tbreak=Tbreak)\n"
        )
    if analyte_conc is None:
        raise Exception(
            "\n**cannot do Rfit with analyte_conc set to None**\nUse fit_fun=lambda t,p : Rfit(t, p, analyte_conc=analyte_conc,Tbreak=Tbreak)\n"
        )
    if T0 is None:
        T0 = times[0]  # default option
    Ybaseline = numpy.array([Y0 for t in times[times < T0]])

    if not hasattr(analyte_conc, "__len__"):
        analyte_concs = [analyte_conc]
    else:
        analyte_concs = list(analyte_conc)

    if not hasattr(Tbreak, "__len__"):
        t_breaks = [T0, Tbreak]
    else:
        t_breaks = list(Tbreak)

    if t_breaks[-1] < times[-1]:
        t_breaks += [times[-1]]
    if t_breaks[0] > times[0]:
        t_breaks = [times[0]] + t_breaks
    if len(t_breaks) > 2 * len(analyte_concs) + 1:
        sys.stderr.write(
            "**ERROR** in concentration_series_fit() len(t_breaks)>2*len(analyte_concs)+1 len(t_breaks)=%d len(analyte_concs)=%d - have you set fit_allow_extra_fraction=0?\n"
            % (len(t_breaks), len(analyte_concs))
        )

    # FORCE some boundaries
    if any(numpy.array(parameters) < 0):
        return 1e5 * numpy.ones(times.shape)

    nextY0 = Y0
    base = 0 # starting point of association phase (first should be zero but then one need to adjust in a multi-concentration single-sensor fit)
    vals = None
    if local_params is not None:
        nloc_params = len(local_params) // len(analyte_concs)
        if len(local_params) % len(analyte_concs) != 0:
            raise Exception(
                "in Rfit local_params given but its length of %d is not a multiple of the number of analyte_concs %d"
                % (len(local_params), len(analyte_concs))
            )
    
    for j, t0 in enumerate(t_breaks[:-1]):
        params = [analyte_concs[j // 2]] + list(parameters)  # pack parameters
        if local_params is not None:  # add local parameters if any
            params += list( local_params[nloc_params * (j // 2) : nloc_params * (j // 2 + 1)] )
        # print ("Deb:",params)
        if (t0 == t_breaks[j + 1]):  # maybe there are multiple associations but not a dissociation in between
            continue
        if j % 2 == 0:  # this is an association phase
            tass = times[((times >= t0) & (times <= t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tass[0] > t0:
                tass = numpy.insert(tass, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (tass[-1] < t_breaks[j + 1]):  # important to add to get correct boundary conditions for later
                tass = numpy.append(tass, [t_breaks[j + 1]])
                pop_one = True
            Yass = base + association_function(tass, params, Y0=nextY0, T0=t0)
            # retrive correct initial conditions for next phase
            nextY0 = Yass[-1]
            if rem_first:  # adjust back so equal number of points as in input times
                Yass = Yass[1:]
            if pop_one:
                Yass = Yass[:-1]
            # add to fitted/theoretical trace
            if len(Yass.shape) > 1:  # probably a bivalent binding
                if vals is None:
                    if len(Ybaseline) > 0:
                        vals = numpy.vstack((Ybaseline, Yass))
                    else:
                        vals = Yass
                else:
                    vals = numpy.vstack((vals, Yass))
            elif vals is None:
                vals = numpy.hstack((Ybaseline, Yass))  # here if Ybaseline is empty no problem
            else:
                vals = numpy.hstack((vals, Yass))

        else:  # dissociation phase
            # select right times for dissociation phase
            tdiss = times[((times > t0) & (times < t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tdiss[0] > t0:
                tdiss = numpy.insert(tdiss, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (tdiss[-1] < t_breaks[j + 1]):  # important to add to get correct boundary conditions for later
                tdiss = numpy.append(tdiss, [t_breaks[j + 1]])
                pop_one = True
            Ydiss = base + dissociation_function(tdiss, params, Ytbreak=nextY0, Tbreak=t0, Ytinf=Ytinf)
            # retrive correct initial conditions for next phase
            nextY0 = Ydiss[-1]
            if rem_first:
                Ydiss = Ydiss[1:]
            if pop_one:
                Ydiss = Ydiss[:-1]
            # add to fitted/theoretical trace
            if len(Ydiss.shape) > 1:  # probably a bivalent binding
                if vals is None:
                    if len(Ybaseline) > 0:
                        vals = numpy.vstack((Ybaseline, Ydiss))
                    else:
                        vals = Ydiss
                else:
                    vals = numpy.vstack((vals, Ydiss))
            elif vals is None:
                vals = numpy.hstack( (Ybaseline, Ydiss) )  # here if Ybaseline is empty no problem
            else:
                vals = numpy.hstack((vals, Ydiss))

    if ( len(vals) == len(times) - 1 ):  # we missed the last tbreak as we removed it but first we excluded it with <
        if len(vals.shape) > 1:
            vals = numpy.vstack((vals, nextY0))
        else:
            vals = numpy.append(vals, nextY0)  # but we saved it anyways
    if len(vals.shape) > 1:  # probably a bivalent binding
        vals = vals.T
        R = vals.sum(axis=0)
        if full_return:
            return R, vals
        return R
    return vals






### COVALENT ASSOCIATION ###
def Rass_covalent(t, params, Y0=0, T0=0):
    '''
    for covalent or very tight association where kdiss=0 is suitable
    can be used for quantitation in spytag-spycatcher system
    params is conc_times_kass, Rmax = params
    '''
    # unpack parameters
    #print("DEB: len(params)",len(params))
    if len(params)==2 :
        conc_times_kass, Rmax = params # fit also Ytinf (here does nothing as this is association)
    else :
        sys.stderr.write("**ERROR** in Rass_covalent expecting two parameters (conc_times_kass, Rmax = params) but found %d\n"%(len(params))) 
        conc_times_kass, Rmax = params[:2]
    if conc_times_kass <= 0 or Rmax <= 0:  # sentinel for boundaries
        return 1e7 * numpy.ones(t.shape)
    # with kdiss=0 Req=Rmax in reality
    Req_cov = Rmax  # in theory it is = Rmax / (1.0 + kdiss / (kass * conc)), but kdiss=0
    return (Y0 - Req_cov) * numpy.exp( -(conc_times_kass ) * (t - T0)) + Req_cov




### HETHEROGENEOUS SURFACE MODEL ###


positive = lambda x: x if x > 0 else 0


def Rass_het(t, params, Y0=0, T0=0):
    # INPUTTING  concentrations in uM and changing limits_kon should give koff and kon in similar numerical ranges which is better for integration
    (
        conc,
        mean_kass,
        mean_kdiss,
        sigma_kass,
        sigma_kdiss,
        Rmax,
    ) = params  # unpack parameters
    if (
        mean_kass < 0 or mean_kdiss < 0 or sigma_kass < 0 or sigma_kdiss < 0 or Rmax < 0
    ):  # sentinel
        return 1e7 * numpy.ones(t.shape)
    limits_kass = (positive(mean_kass - 3.5 * sigma_kass), mean_kass + 3.5 * sigma_kass)
    limits_kdiss = (
        positive(mean_kdiss - 3.5 * sigma_kdiss),
        mean_kdiss + 3.5 * sigma_kdiss,
    )
    cov = numpy.array(
        [[sigma_kass ** 2, 0], [0, sigma_kdiss ** 2]]
    )  # covariance between x1 and x2 set to 0 so that these 2 variables are independen
    normal2d = scipy.stats.multivariate_normal(
        mean=numpy.array([mean_kass, mean_kdiss]), cov=cov
    )
    # give the normal2d as Rmax to association, then fit parameters - the weights are the number of binding sites
    inner_to_integrate = lambda kass_x, kdiss_x, times: Rass(
        times,
        [conc, kass_x, kdiss_x, normal2d.pdf(numpy.array([kass_x, kdiss_x]))],
        Y0=Y0,
        T0=T0,
    )
    # dblquad is designed to use as integration limits of the second variable functions of the first one,
    #  so when these are constant, callable constant functions must be defined
    all_with_err = [
        scipy.integrate.dblquad(
            inner_to_integrate,
            limits_kass[0],
            limits_kass[1],
            lambda x: limits_kdiss[0],
            lambda x: limits_kdiss[1],
            args=(tim,),
        )
        for tim in t
    ]
    return Rmax * numpy.array(all_with_err).T[0], Rmax * numpy.array(all_with_err).T[1]


precoumputed_X_W_discrete = {  # to avoid recomputing multiple times
    "X_W": None,
    "mean_kass": None,
    "mean_kdiss": None,
    "sigma_kass": None,
    "sigma_kdiss": None,
    "last_func_t": None,
    "npoints": 0,
    "nsigma": 0,
    "timechange": 1e-3,
    "concchange": 1e6,
}


def _compute_discrete_weigths(
    mean_kass,
    mean_kdiss,
    sigma_kass,
    sigma_kdiss,
    rho,
    npoints=11,
    nsigma=3,
    allow_singular=True,
    debug=False,
):
    # allow_singular is far from ideal but otherwise it often rounds it to eventually zero and crashes
    if (
        precoumputed_X_W_discrete["X_W"] is None
        or precoumputed_X_W_discrete["mean_kass"] != mean_kass
        or precoumputed_X_W_discrete["mean_kdiss"] != mean_kdiss
        or precoumputed_X_W_discrete["sigma_kass"] != sigma_kass
        or precoumputed_X_W_discrete["sigma_kdiss"] != sigma_kdiss
        or precoumputed_X_W_discrete["npoints"] != npoints
        or precoumputed_X_W_discrete["nsigma"] != nsigma
    ):

        kass_points = numpy.linspace(
            positive(mean_kass - nsigma * sigma_kass) + 1e-4,
            mean_kass + nsigma * sigma_kass,
            npoints,
        )
        kdiss_points = numpy.linspace(
            positive(mean_kdiss - nsigma * sigma_kdiss) + 1e-8,
            mean_kdiss + nsigma * sigma_kdiss,
            npoints,
        )

        cov = numpy.array(
            [
                [sigma_kass ** 2, rho * sigma_kass * sigma_kdiss],
                [rho * sigma_kass * sigma_kdiss, sigma_kdiss ** 2],
            ]
        )  # covariance between x1 and x2 set to 0 so that these 2 variables are independent
        # DEB:
        if debug:
            print("DEB cov:", cov)
        normal2d = scipy.stats.multivariate_normal(
            mean=numpy.array([mean_kass, mean_kdiss]),
            cov=cov,
            allow_singular=allow_singular,
        )
        # Old way beter for 3D plots
        # x1, x2 = numpy.meshgrid(kass_points, kdiss_points) # Generate grid
        # in this way you can view the weigths as a matrix, which is more intuitive for 3d plots
        # Z=normal2d.pdf(numpy.array([x1,x2]).T) # get PDF (so that sums zero from area)

        # generate grid, and convert to pairs of (kass, kdiss) all combinations
        X = numpy.array(numpy.meshgrid(kass_points, kdiss_points)).T.reshape(-1, 2)
        # get weights, which will be used as Rmax
        W = normal2d.pdf(X)
        W /= W.sum()  # normalise, so that weights sum to zero
        precoumputed_X_W_discrete["X_W"] = (X, W)
        precoumputed_X_W_discrete["mean_kass"] = mean_kass
        precoumputed_X_W_discrete["mean_kdiss"] = mean_kdiss
        precoumputed_X_W_discrete["sigma_kass"] = sigma_kass
        precoumputed_X_W_discrete["sigma_kdiss"] = sigma_kdiss
        precoumputed_X_W_discrete["npoints"] = npoints
        precoumputed_X_W_discrete["nsigma"] = nsigma
    return precoumputed_X_W_discrete["X_W"]


def Rass_het_discrete(
    t,
    params,
    Y0=0,
    T0=0,
    npoints=11,
    nsigma=3,
    change_units_to_regularise=True,
    full_return=False,
):
    # works but association output INCREDIBLY close to normal Rass with means given as Kass and Kdiss
    # with full return it will return npoints**2 traces as well as their sum which is Rtot
    if len(params) == 6:
        rho = 0
        (
            conc,
            mean_kass,
            mean_kdiss,
            sigma_kass,
            sigma_kdiss,
            Rmax,
        ) = params  # unpack parameters
    else:  # rho is present
        (
            conc,
            mean_kass,
            mean_kdiss,
            sigma_kass,
            sigma_kdiss,
            rho,
            Rmax,
        ) = params  # unpack parameters
    if (
        mean_kass < 0 or mean_kdiss < 0 or sigma_kass < 0 or sigma_kdiss < 0 or Rmax < 0
    ):  # sentinel
        return 1e7 * numpy.ones(t.shape)
    if npoints % 2 == 0:
        npoints += 1
    if (
        change_units_to_regularise
    ):  # to avoid singular matrices (which happens because the multivariate_normal rounds things)
        conc *= precoumputed_X_W_discrete["concchange"]  # Molar to Micromolar
        times = t * precoumputed_X_W_discrete["timechange"]  # seconds to KiloSeconds
        T0 *= precoumputed_X_W_discrete["timechange"]
        mean_kdiss /= precoumputed_X_W_discrete["timechange"]
        sigma_kdiss /= precoumputed_X_W_discrete["timechange"]
        mean_kass /= (
            precoumputed_X_W_discrete["timechange"]
            * precoumputed_X_W_discrete["concchange"]
        )  # conc*times
        sigma_kass /= (
            precoumputed_X_W_discrete["timechange"]
            * precoumputed_X_W_discrete["concchange"]
        )
    else:
        times = t
    X, W = _compute_discrete_weigths(
        mean_kass,
        mean_kdiss,
        sigma_kass,
        sigma_kdiss,
        rho,
        npoints=npoints,
        nsigma=nsigma,
    )
    if hasattr(Y0, "__len__") and len(Y0) == len(
        W
    ):  # you have detailed initial conditions of individual weights
        precoumputed_X_W_discrete["last_func_t"] = lambda tt: numpy.array(
            [
                Rass(tt, [conc, kas_kdis[0], kas_kdis[1], Rmax * W[j]], Y0=Y0[j], T0=T0)
                for j, kas_kdis in enumerate(X)
            ]
        )
    else:
        precoumputed_X_W_discrete["last_func_t"] = lambda tt: numpy.array(
            [
                Rass(
                    tt,
                    [conc, kas_kdis[0], kas_kdis[1], Rmax * W[j]],
                    Y0=Y0 * W[j],
                    T0=T0,
                )
                for j, kas_kdis in enumerate(X)
            ]
        )
    Y = precoumputed_X_W_discrete["last_func_t"](times)
    if full_return:
        return (
            Y.sum(axis=0),
            Y.T,
        )  # return transposed for compatibility with biv models so Y[-1] is last timepoint
    return (
        Y.T
    )  # return transposed for compatibility with biv models so Y[-1] is last timepoint


def Rdiss_het_discrete(
    t,
    params,
    Ytbreak=1,
    Tbreak=0,
    Ytinf=0,
    npoints=11,
    nsigma=3,
    change_units_to_regularise=True,
    full_return=False,
):
    # works but association output INCREDIBLY close to normal Rass with means given as Kass and Kdiss
    # with full return it will return npoints**2 traces as well as their sum which is Rtot
    # Ytbreak=1, Tbreak=0, Ytinf=0
    if len(params) == 6:
        rho = 0
        (
            conc,
            mean_kass,
            mean_kdiss,
            sigma_kass,
            sigma_kdiss,
            Rmax,
        ) = params  # unpack parameters
    else:  # rho is present
        (
            conc,
            mean_kass,
            mean_kdiss,
            sigma_kass,
            sigma_kdiss,
            rho,
            Rmax,
        ) = params  # unpack parameters
    if (
        mean_kass < 0 or mean_kdiss < 0 or sigma_kass < 0 or sigma_kdiss < 0 or Rmax < 0
    ):  # sentinel
        return 1e7 * numpy.ones(t.shape)
    if npoints % 2 == 0:
        npoints += 1
    if (
        change_units_to_regularise
    ):  # to avoid singular matrices (which happens because the multivariate_normal rounds things)
        conc *= precoumputed_X_W_discrete["concchange"]  # Molar to Micromolar
        times = t * precoumputed_X_W_discrete["timechange"]  # seconds to KiloSeconds
        Tbreak *= precoumputed_X_W_discrete["timechange"]
        mean_kdiss /= precoumputed_X_W_discrete["timechange"]
        sigma_kdiss /= precoumputed_X_W_discrete["timechange"]
        mean_kass /= (
            precoumputed_X_W_discrete["timechange"]
            * precoumputed_X_W_discrete["concchange"]
        )  # conc*times
        sigma_kass /= (
            precoumputed_X_W_discrete["timechange"]
            * precoumputed_X_W_discrete["concchange"]
        )
    else:
        times = t
    X, W = _compute_discrete_weigths(
        mean_kass,
        mean_kdiss,
        sigma_kass,
        sigma_kdiss,
        rho,
        npoints=npoints,
        nsigma=nsigma,
    )
    if hasattr(Ytbreak, "__len__") and len(Ytbreak) == len(
        W
    ):  # you have detailed initial conditions of individual weights
        precoumputed_X_W_discrete["last_func_t"] = lambda tt: numpy.array(
            [
                Rdiss(
                    tt,
                    [conc, kas_kdis[0], kas_kdis[1], Rmax * W[j]],
                    Ytbreak=Ytbreak[j],
                    Tbreak=Tbreak,
                    Ytinf=Ytinf * W[j],
                )
                for j, kas_kdis in enumerate(X)
            ]
        )
    else:
        precoumputed_X_W_discrete["last_func_t"] = lambda tt: numpy.array(
            [
                Rdiss(
                    tt,
                    [conc, kas_kdis[0], kas_kdis[1], Rmax * W[j]],
                    Ytbreak=Ytbreak * W[j],
                    Tbreak=Tbreak,
                    Ytinf=Ytinf * W[j],
                )
                for j, kas_kdis in enumerate(X)
            ]
        )
    Y = precoumputed_X_W_discrete["last_func_t"](times)
    if full_return:
        return (
            Y.sum(axis=0),
            Y.T,
        )  # return transposed for compatibility with biv models so Y[-1] is last timepoint
    return (
        Y.T
    )  # return transposed for compatibility with biv models so Y[-1] is last timepoint


## BIVALENT BINDING MODEL WITH NUMERIC INTEGRATION ##
def Rass_biv_generic(t, params, Y0=0.0, T0=0):
    """
    # see Eq 7 and 8 of https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4617740/pdf/RSINAK-000086-106107_1.pdf
    # or https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4617740/pdf/RSINAK-000086-106107_1.pdf
    # does not provide the rates
    analyte_conc not actually used here
    """
    analyte_conc, E, F, s1, s2, g1, g2 = params  # g1,g2 are dissociation
    D = -(E + F)
    return D + E * numpy.exp(-s1 * (t - T0)) + F * numpy.exp(-s2 * (t - T0))


def Rdiss_biv_generic(t, params, Ytbreak=1.0, Tbreak=0, Ytinf=0):
    """
    # see Eq 7 and 8 of https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4617740/pdf/RSINAK-000086-106107_1.pdf
    # or https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4617740/pdf/RSINAK-000086-106107_1.pdf
    # does not provide the rates
    analyte_conc not actually used here
    """
    analyte_conc, E, F, s1, s2, g1, g2 = params  # g1,g2 are dissociation
    return E * numpy.exp(-g1 * (t - Tbreak)) + F * numpy.exp(-g2 * (t - Tbreak))


def Rass_biv_ind(t, params, Y0=[0.0, 0.0], T0=0):
    # Y0=[R10,R20] only one Kass
    if not hasattr(Y0, "__len__"):
        raise Exception(
            "to use bivalent models initial conditions Y0 must be e.g. Y0=[0,0] currently Y0=%s"
            % (str(Y0))
        )
    if len(params) == 4:
        conc, kass1, kdiss1, kdiss2, Rmax = params  # unpack parameters
        Weig = 0.5
    else:
        conc, kass1, kdiss1, kdiss2, Rmax, Weig = params  # unpack parameters
        if Weig < 0 or Weig > 1:  # sentinel
            return 1e7 * numpy.ones(t.shape)
    R1 = (Y0[0] - Req(conc, kass1, kdiss1, Rmax * Weig)) * numpy.exp(
        -(kass1 * conc + kdiss1) * (t - T0)
    ) + Req(conc, kass1, kdiss1, Rmax * Weig)
    R2 = (Y0[1] - Req(conc, kass1, kdiss2, Rmax * (1 - Weig))) * numpy.exp(
        -(kass1 * conc + kdiss2) * (t - T0)
    ) + Req(conc, kass1, kdiss2, Rmax * (1 - Weig))
    return numpy.vstack((R1, R2)).T


def Rdiss_biv_ind(t, params, Ytbreak=[1.0, 1.0], Tbreak=0, Ytinf=0):
    # only one Kass
    # Ytbreak=[R10,R20] Tbreak is irrelevant here but the first t of t will be used as Tbreak
    if len(params) == 4:
        conc, kass1, kdiss1, kdiss2, Rmax = params  # unpack parameters
        Weig = 0.5
    else:
        conc, kass1, kdiss1, kdiss2, Rmax, Weig = params  # unpack parameters
        if Weig < 0 or Weig > 1:  # sentinel
            return 1e7 * numpy.ones(t.shape)
    R1 = (Ytbreak[0] - Ytinf) * numpy.exp(-(kdiss1) * (t - Tbreak)) + Ytinf
    R2 = (Ytbreak[1] - Ytinf) * numpy.exp(-(kdiss2) * (t - Tbreak)) + Ytinf
    return numpy.vstack((R1, R2)).T


def fun_bivalent_association(y, t, params):
    """
    # Equations taken from (1) Cooper, M. A.; Williams, D. H. Kinetic Analysis of Antibody–Antigen Interactions at a Supported Lipid Monolayer. Analytical Biochemistry 1999, 276 (1), 36–47. https://doi.org/10.1006/abio.1999.4333.
    # dR1_dt = (2*kass1* conc * (Rmax - R1 - 2*R2) - kdiss1*R1) - (kass2 * R1 * (Rmax-R1-2*R2) - 2*kdiss2*R2)
    # dR2_dt = kass2 * R1 * (Rmax-R1-2*R2) - 2*kdiss2*R2
    """
    R1, R2 = y  # unpack current values of y
    conc, kass1, kdiss1, kass2, kdiss2, Rmax = params  # unpack parameters
    derivs = [  # list of dy/dt=f functions:
        (2 * kass1 * conc * (Rmax - R1 - 2 * R2) - kdiss1 * R1)
        - (kass2 * R1 * (Rmax - R1 - 2 * R2) - 2 * kdiss2 * R2),  # =dR1_dt
        kass2 * R1 * (Rmax - R1 - 2 * R2) - 2 * kdiss2 * R2,  # =dR2_dt
    ]
    return derivs


def Rass_biv(t, params, Y0=[0.0, 0.0], T0=0, base=0):
    # Y0=[R10,R20] T0 is irrelevant here but the first t of t will be used as T0
    if not hasattr(Y0, "__len__"):
        raise Exception(
            "to use bivalent models initial conditions Y0 must be e.g. Y0=[0,0] currently Y0=%s"
            % (str(Y0))
        )
    R1_R2_association = base + scipy.integrate.odeint(
        fun_bivalent_association, Y0, t, args=(params,)
    )
    return R1_R2_association


# Dissociation
def fun_bivalent_dissociation(
    y, t, params
):  # parameters are same as association but conc and kass1 are not used
    """
    # MODEL model the [LLA] <--> [L]+[LA] --> 2[L]+[A]
    # dR2_dt = kass2 * R1 * (Rmax-R1-R2) - 2*kdiss2*R2
    # dR1_dt = -kdiss1*R1 - (kass2 * R1 * (Rmax-R1-R2) - 2*kdiss2*R2 ) first bit would be ==> R1(t)= R10 * numpy.exp(-kdiss1*(t-t0))
    """
    R1, R2 = y  # unpack current values of y
    conc, kass1, kdiss1, kass2, kdiss2, Rmax = params  # unpack parameters
    derivs = [  # list of dy/dt=f functions:
        -kdiss1 * R1 - (kass2 * R1 * (Rmax - R1 - R2) - 2 * kdiss2 * R2),  # =dR1_dt
        kass2 * R1 * (Rmax - R1 - R2) - 2 * kdiss2 * R2,  # =dR2_dt
    ]
    return derivs


def Rdiss_biv(t, params, Ytbreak=[1.0, 1.0], Tbreak=0, Ytinf=0, base=0):
    # Ytbreak=[R10,R20] Tbreak is irrelevant here but the first t of t will be used as Tbreak
    R1_R2_dissociation = (
        base
        + scipy.integrate.odeint(
            fun_bivalent_dissociation, (Ytbreak - Ytinf), t, args=(params,)
        )
        + Ytinf
    )
    return R1_R2_dissociation


def bivalent_binding(t, p, analyte_conc=None, Tbreak=None, full_return=False):
    """
    SET fit_allow_extra_fraction=0 before using in fits
    parameters_name = [ 'kass1 (1/Ms)', 'kdiss1 (1/s)', 'kass2 (1/Ms)', 'kdiss2 (1/s)', 'Rmax' ] = p
    # full_return will return R, R1, R2
    # else returns R (R=R1+R2) which is what is read experimentally (so for fitting, full_return=True is for analysis)
    """
    if analyte_conc is None:
        raise Exeption("analyte_conc is required to use function bivalent_binding\n")
    if Tbreak is None:
        raise Exeption("Tbreak is required to use function bivalent_binding\n")
    params = [analyte_conc] + list(p)[:5]  # pack parameters
    if (
        len(p) > 5
    ):  # extra parameter that may be useful to shift association to account for possible differences in refractive index due to presence of analyte
        base = p[5]
    else:
        base = 0
    # INITIAL values (boundary conditions)
    R10 = 0
    R20 = 0
    y0 = [R10, R20]
    # times for association
    t = numpy.array(t)
    tass = t[t <= Tbreak]
    pop_one = False
    if tass[-1] < Tbreak:
        tass = numpy.append(tass, Tbreak)
        # print('pop_one',len(tass),len(t))
        pop_one = True
    R1_R2_association = base + scipy.integrate.odeint(
        fun_bivalent_association, y0, tass, args=(params,)
    )
    # initial values for dissociation
    R10, R20 = R1_R2_association[-1]  # set to end of association phase
    if pop_one:
        R1_R2_association = R1_R2_association[:-1]
    # Bundle initial conditions for ODE solver
    y0 = [R10, R20]
    # times for dissociation
    tdiss = t[t > Tbreak]
    rem_first = (
        False  # useful to make sure the boundary conditions are at the right time
    )
    if tdiss[0] > Tbreak:
        tdiss = numpy.insert(tdiss, 0, Tbreak)
        # print('rem_first',len(tdiss),len(tass),len(t))
        rem_first = True
    R1_R2_dissociation = scipy.integrate.odeint(
        fun_bivalent_dissociation, y0, tdiss, args=(params,)
    )
    if rem_first:
        R1_R2_dissociation = R1_R2_dissociation[1:]
    R1_R2 = numpy.vstack((R1_R2_association, R1_R2_dissociation)).T
    R = R1_R2.sum(axis=0)
    if full_return:
        return R, R1_R2[0], R1_R2[1]
    return R


def bivanent_concentration_series_fit(
    t, p, analyte_concs=None, t_breaks=None, full_return=False
):
    """
    CAREFUL works only with fit_allow_extra_fraction=0
    t_breaks should be all breaks between association/dissociation and dissociation/next association
    parameters_name = [ 'kass1 (1/Ms)', 'kdiss1 (1/s)', 'kass2 (1/Ms)', 'kdiss2 (1/s)', 'Rmax' ] = p
    # full_return will return R, R1, R2
    # else returns R (R=R1+R2) which is what is read experimentally (so for fitting, full_return=True is for analysis)
    """
    if t_breaks is None:
        raise Exception(
            "\n**cannot do concentration_series_fit with t_breaks set to None**\nUse fit_fun=lambda t,p : concentration_series_fit(t, p, analyte_concs=analyte_concs,t_breaks=t_breaks)\n"
        )
    if analyte_concs is None:
        raise Exception(
            "\n**cannot do concentration_series_fit with t_breaks set to None**\nUse fit_fun=lambda t,p : concentration_series_fit(t, p, analyte_concs=analyte_concs,t_breaks=t_breaks)\n"
        )
    t_breaks = list(t_breaks)
    if t_breaks[-1] < t[-1]:
        t_breaks += [t[-1]]
    if t_breaks[0] > t[0]:
        t_breaks = [t[0]] + t_breaks
    if len(t_breaks) > 2 * len(analyte_concs) + 1:
        sys.stderr.write(
            "**ERROR** in concentration_series_fit() len(t_breaks)>2*len(analyte_concs)+1 len(t_breaks)=%d len(analyte_concs)=%d - have you set fit_allow_extra_fraction=0?\n"
            % (len(t_breaks), len(analyte_concs))
        )
    Ytinf = 0
    R10 = 0
    R20 = 0
    all_R1_R2 = None
    t = numpy.array(t)  # start from zero
    # FORCE some boundaries
    if any(numpy.array(p) < 0):
        return 1e9 * numpy.ones(t.shape)
    # print('DEB',len(t_breaks),t_breaks)
    for j, t0 in enumerate(t_breaks[:-1]):
        params = [analyte_concs[j // 2]] + list(p)[:5]  # pack parameters
        if len(p) == 6:
            base = (
                analyte_concs[j // 2] * p[5]
            )  # one base, refractive index assumed to scale linearly with analyte conc.
        elif len(p) > 6:
            base = p[5 + j // 2]  # each analyte conc has own base
        else:
            base = 0
        if j % 2 == 0:
            # Bundle initial conditions for ODE solver
            y0 = [R10, R20]
            # select right times for association phase
            tass = t[((t >= t0) & (t <= t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tass[0] > t0:
                tass = numpy.insert(tass, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (
                tass[-1] < t_breaks[j + 1]
            ):  # important to add to get correct boundary conditions for later
                tass = numpy.append(tass, [t_breaks[j + 1]])
                pop_one = True
            # integrate ODE
            R1_R2_association = base + scipy.integrate.odeint(
                fun_bivalent_association, y0, tass, args=(params,)
            )
            # retrieve initial conditions for next phase
            R10, R20 = R1_R2_association[
                -1
            ]  # initial condition will be end of association phase estimated exactly at Tbreak
            if rem_first:
                R1_R2_association = R1_R2_association[1:]
            if pop_one:
                R1_R2_association = R1_R2_association[:-1]
            # add to fitted/theoretical trace
            if all_R1_R2 is None:
                all_R1_R2 = R1_R2_association
            else:
                all_R1_R2 = numpy.vstack((all_R1_R2, R1_R2_association))
        else:
            # Bundle initial conditions for ODE solver
            y0 = [R10, R20]
            # select right times for dissociation phase
            tdiss = t[((t > t0) & (t < t_breaks[j + 1]))]
            rem_first = False  # useful to make sure the boundary conditions are at the right time
            if tdiss[0] > t0:
                tdiss = numpy.insert(tdiss, 0, t0)
                # print('rem_first',len(tdiss),len(tass),len(t))
                rem_first = True
            pop_one = False
            if (
                tdiss[-1] < t_breaks[j + 1]
            ):  # important to add to get correct boundary conditions for later
                tdiss = numpy.append(tdiss, [t_breaks[j + 1]])
                pop_one = True
            # integrate ODE
            R1_R2_dissociation = scipy.integrate.odeint(
                fun_bivalent_dissociation, y0, tdiss, args=(params,)
            )
            # retrieve initial conditions for next phase
            R10, R20 = R1_R2_dissociation[-1]
            if rem_first:
                R1_R2_dissociation = R1_R2_dissociation[1:]
            if pop_one:
                R1_R2_dissociation = R1_R2_dissociation[:-1]
            # add to fitted/theoretical trace
            all_R1_R2 = numpy.vstack((all_R1_R2, R1_R2_dissociation))
    R1, R2 = all_R1_R2.T
    if (
        len(R1) == len(t) - 1
    ):  # we missed the last tbreak as we removed it but first we excluded it with <
        R1 = numpy.append(R1, R10)  # but we saved it anyways
        R2 = numpy.append(R2, R20)  # but we saved it anyways
    R = R1 + R2  # total theoretical trace, which should fit observed trace
    if full_return:
        return R, R1, R2
    return R


def read_rawdata_file_New_Program(fname, key_column_hd_name="Time (s)"):
    """
    reads .csv raw data file as exported by the new K2 BLI data program in the export pre-processed data
    """
    data = csv_dict.Data()
    data.filename = fname
    data.key_column_hd_name = key_column_hd_name
    with open(fname) as fff:
        lines = fff.read().splitlines()
    HD = None
    for j, l in enumerate(lines):
        try:
            if len(l) < 2:
                continue
            if "X Values" in l or j == 0 or l.lower().startswith('time') :
                if HD is not None and HD != []:
                    print("**ERROR** overwriting HD (formerly %s)" % (str(HD)))
                if "," in l:
                    HD = list(map(str.strip, l.strip().strip(",").split(",")[1:]))
                else:
                    HD = l.split()[1:]
                continue
            if not l[0].isdigit():
                print(("skip:", l))
                continue
            if "," in l:
                entries = list(map(float, l.strip().strip(",").split(",")))
            else:
                entries = list(map(float, l.split()))
            if entries[0] in data:
                print(("**WARNING** overwriting time", entries[0], "line", j, l))
            data[entries[0]] = entries[1:]
        except Exception:
            print(("RAISED at line:", j))
            print(l)
            raise
    if HD is None:
        print(("**ERROR** could not find HD", fname))
    else:
        data.hd = csv_dict.list_to_dictionary(HD)
    return data


def read_AssaySteps_New_Program(assay_step_table, return_value_labels=True):
    """
    reads the assay step table:
     In New Program in the tool bar of process data there is the option Export Excel Report - here select the experiment summary and the plate and sensor images and details.
     The Sample Plate info contain in a rather weird way a table with sample names and concentrations (as long as at least one association was done there I guess)
     What one really needs is the Experiment Summary and in particular the Assay Step Info: table 
     Copy in a new file Assay_step.txt the Assay Step Info table from the first sheet of the Excel Report
    return_value_labels returns a dictionary for plots, where keys are mid-time of phase and values 
    are phase key\nphase type (type being baseline, association, dissociation, etc...)
    """
    assay_steps = csv_dict.Data(fname=assay_step_table, key_column=None)
    for k in list(assay_steps.hd.keys()):
        nk = k.strip()
        if nk != k:
            assay_steps.hd[nk] = assay_steps.hd[k]
            del assay_steps.hd[k]
    t = 0
    for k in assay_steps:
        assay_steps[k] += [t]
        t += assay_steps[k][assay_steps.hd["Step Time"]]
        assay_steps[k] += [t]
    assay_steps._update_hd("Step Begins")
    assay_steps._update_hd("Step Ends")
    if return_value_labels:
        value_labels = OrderedDict()
        for sn in assay_steps:
            t = (
                assay_steps[sn][assay_steps.hd["Step Begins"]]
                + assay_steps[sn][assay_steps.hd["Step Ends"]]
            ) / 2.0
            value_labels[t] = (
                str(sn) + "\n" + assay_steps[sn][assay_steps.hd["Step Name"]]
            )
        return assay_steps, value_labels
    return assay_steps


def separate_into_phases(
    raw_data,
    assay_steps,
    add_smoothed=True,
    window_length=11,
    polyorder=2,
    value_labels=None,
    save=None,
):
    """
    returns data_by_phase, data_by_phase_by_sensors - an OrderedDict where keys are assay_steps key
     if add_smoothed adds two other columns with Smoothed + HDname
    data_by_phase_by_sensors is a dictionary of dictionary of numpy array to facilitate subtractions and fits
    if save it also prints the data_by_phase_by_sensors in a file (one entry per line)
     value_labels if given can be used to enrich this printout
    """
    data_by_phase = OrderedDict()
    step_begins = []
    for sn in assay_steps:
        for (
            t
        ) in raw_data:  # weird but needs round otherwise it separates at wrong place!
            if round(t, 2) >= round(
                assay_steps[sn][assay_steps.hd["Step Begins"]], 2
            ) and round(t, 2) < round(assay_steps[sn][assay_steps.hd["Step Ends"]], 2):
                if sn not in data_by_phase:
                    data_by_phase[sn] = raw_data.copy(deep=False)
                    step_begins += [t]
                    print(
                        (
                            "step %s begins at %g s , tabulated %lf s"
                            % (sn, t, assay_steps[sn][assay_steps.hd["Step Begins"]])
                        )
                    )
                data_by_phase[sn][t] = raw_data[t][:]
    if add_smoothed:
        for sn in list(data_by_phase.keys()):
            delete_phase=False
            for c in data_by_phase[sn].HD():
                prof_to_smooth = numpy.array(data_by_phase[sn].filter_null(c)[0][0])
                if len(prof_to_smooth)==0 :
                    sys.stderr.write("\n**BIG WARNING** phase %s has %d data point, deleting it!\n\n"% (sn,len(prof_to_smooth)))
                    delete_phase=True
                    break
                if len(prof_to_smooth) < window_length * 2:
                    print(
                        (
                            "Potential WARNING phase %s has %d data point but smooth window_length is %d --> setting window_length=%d [sensor %s]\n"
                            % (
                                sn,
                                len(prof_to_smooth),
                                window_length,
                                2 * int(len(prof_to_smooth) / 4) + 1,
                                c,
                            )
                        )
                    )
                    wl = 2 * int(len(prof_to_smooth) / 4) + 1
                else:
                    wl = window_length
                sm = misc.smooth_profile(
                    prof_to_smooth,
                    use_savgol_filter=(wl, polyorder),
                )  # scipy.signal.savgol_filter(numpy.array(data_by_phase[sn].filter_null(c)[0][0]) , window_length=wl,polyorder=polyorder)
                data_by_phase[sn].add_column(sm, "Smoothed " + c, verbose=False)
            if delete_phase :
                del data_by_phase[sn] 
                continue
    data_by_phase_by_sensors = OrderedDict()
    for ph in data_by_phase:
        data_by_phase_by_sensors[ph] = OrderedDict()
        data_by_phase_by_sensors[ph]["Times"] = numpy.array( [float(x) for x in data_by_phase[ph].keys() if not (x=='' or numpy.isnan(x)) ] )
        for sens in data_by_phase[ph].HD():
            data_by_phase_by_sensors[ph][sens] = numpy.array( [float(x) for x in data_by_phase[ph].column(sens) if not (x=='' or numpy.isnan(x)) ] )
            if len(data_by_phase_by_sensors[ph][sens]) != len(data_by_phase_by_sensors[ph]["Times"]) :
                sys.stderr.write("**potential issue** separate_into_phases() when creating data_by_phase_by_sensors, non-nan length of sensor %s is %d, but length of times is %d (should be equal! Cropping to length of shorter!)\n"%(sens,len(data_by_phase_by_sensors[ph][sens]),len(data_by_phase_by_sensors[ph]["Times"])))
                if len(data_by_phase_by_sensors[ph][sens]) < len(data_by_phase_by_sensors[ph]["Times"]) :
                    data_by_phase_by_sensors[ph]["Times"]= data_by_phase_by_sensors[ph]["Times"][:len(data_by_phase_by_sensors[ph][sens])]
                else :
                    data_by_phase_by_sensors[ph][sens]=data_by_phase_by_sensors[ph][sens][:len(data_by_phase_by_sensors[ph]["Times"])]
    if save is not None:
        if ".csv" != save[-4:]:
            save += ".csv"
        out = open(save, "w")
        for ph in data_by_phase_by_sensors:
            out.write("%d" % (ph))
            if value_labels is not None:
                phid = None
                for k in value_labels:
                    if "\n" in value_labels[k]:
                        phid, phname = value_labels[k].split("\n")
                    elif ":" in value_labels[k]:
                        phid, phname = value_labels[k].split(":")
                    elif "-" in value_labels[k]:
                        phid, phname = value_labels[k].split("-")
                    phid = int(phid)
                    if phid == ph:
                        break
                if phid is not None:
                    out.write(",%s" % (phname))
                else:
                    out.write(",")
            for k2 in data_by_phase_by_sensors[ph]:
                if k2 != "Times":
                    out.write(",")
                out.write(
                    ",%s,%s\n"
                    % (k2, ",".join(map(str, data_by_phase_by_sensors[ph][k2])))
                )
        out.close()
    return data_by_phase, data_by_phase_by_sensors


# def create_all_sensors_phase_dict(data_by_phase_by_sensors,value_labels=None) :
#    all_sensors_phase_dict_raw={}
##    all_sensors_phase_dict_smooth={}
#    for ph in data_by_phase_by_sensors :
#        if value_labels is not None :
#            phid=None
#            for k in value_labels :
#                if '\n' in value_labels[k] : phid,phname=value_labels[k].split('\n')
#                elif ':' in value_labels[k] : phid,phname=value_labels[k].split(':')
#                elif '-' in value_labels[k] : phid,phname=value_labels[k].split('-')
#                phid=int(phid)
#                if phid==ph : break
#            if phid is not None : phname
#    return

def phase_dict_to_csv_dict(phase_dict, phase_dict_name='', add_phase_name_column=True, merge_into_existing=None) :
    '''
    print_phase_dict
    phase_dict_name can be e.g. sensor name (Ref, Antibody1, etc.) it will be added to the header
    phase_dict is a dict of phases like 
            OrderedDict([('baseline',
                   [array([-2.700e+02, -2.698e+02, -2.696e+02, ..., -6.000e-01, -4.000e-01,
                           -2.000e-01]), # Time
                    array([ 0.01716316,  0.02013672,  0.02238072, ..., -0.00371086,
                           -0.00779008, -0.00475883])]), # Signal
                  ('association',
                   [array([0.000e+00, 2.000e-01, 4.000e-01, ..., 2.994e+02, 2.996e+02,
                           2.998e+02]), # Time
                    array([-0.00492095, -0.00578546, -0.00700473, ...,  0.1398579 ,
                            0.1360153 ,  0.13521434])]), # Signal
    '''
    dat = csv_dict.Data()
    dat.key_column_hd_name='Time (s)'
    if add_phase_name_column :
        dat.hd=csv_dict.list_to_dictionary([phase_dict_name+'Phase_ind', phase_dict_name+'Phase_name', phase_dict_name+'Signal (nm)'])
    else : 
        dat.hd=csv_dict.list_to_dictionary([phase_dict_name+'Signal (nm)'])
    for jp,phase in enumerate(phase_dict) :
        for j,t in enumerate(phase_dict[phase][0]) : # loop on times
            if t in dat : 
                sys.stderr.write("**ERROR** in phase_dict_to_csv_dict overwriting time %s\n"%(str(t)))
            if add_phase_name_column : 
                dat[t]=[ jp, phase, phase_dict[phase][1][j] ]
            else : 
                dat[t]=[ phase_dict[phase][1][j] ]
    if merge_into_existing is not None and merge_into_existing!=False :
        merge_into_existing.merge(dat)
        return merge_into_existing
    return dat

def align_phase_dict(
    dict_of_phases,
    smoothed_dict_to_use=None,
    all_merged=True,
    association_begins_at_zero=True,
    association_start_time_to_zero=False,
    align_to_begin_of_phase=None,
    align_to_phase='baseline',
    print_k='',
):
    """
    aligns all to the average of the last 1/3 of points in the phase align_to_phase (typically the baseline)
    if align_to_begin_of_phase it will ignore align_to_phase and all phases are aligned at beginning of input phase instead of association 
      (i.e. association_begins_at_zero and association_start_time_to_zero then apply to that phase)
    all_merged makes it so that every phase begins at the y-value where the previous one ends (by vertical translation of all data points)
    association_begins_at_zero shift everything (all phases) vertically so that the first y-value of the association is zero
      when False the mean of the second half of the baseline will be subtracted
      quite generally when the signal is low and association very fast this is better set to False
    requires OrderedDict as input for dict_of_phases to work properly
    """
    aligned_phases = OrderedDict()
    aligned_smoothed_phases = OrderedDict()
    # start from first phase, align second to beginning of first and so on
    if not isinstance(dict_of_phases, OrderedDict):
        print( "**POTENTIAL ERROR (but only python 2) in align_phase_dict input dict_of_phases is not OrderedDict - phases will be aligned in this order:%s\n"
                % (str(list(dict_of_phases.keys()))) )
    # identify first association phase
    ass_phase = [a for a in dict_of_phases if a.lower().startswith('association')]
    if association_begins_at_zero or association_start_time_to_zero :
        if ass_phase==[] :
            sys.stderr.write("**ERROR** in align_phase_dict(%s) cannot automatically identify association phase! DOING NOTHGIN"%(print_k))
            return dict_of_phases
        association= ass_phase[0]
    # check consistency of input
    if align_to_begin_of_phase is not None and association_begins_at_zero and align_to_begin_of_phase!=association:
        sys.stderr.write("**WARNING** input inconsistency, align_to_begin_of_phase(%s) given as %s but also association_begins_at_zero=True (now setting to False!)"%(print_k,align_to_begin_of_phase))
        association_begins_at_zero=False
    if association_begins_at_zero :
        align_to_begin_of_phase = association
    
    subtract_time=0
    if association_start_time_to_zero :
        subtract_time = dict_of_phases[association][0][0]
    
    # first merge them all if needed and apply time offset if asked
    off_from_last_phase=0 # so first phase never shifts, will be non-zero later if all_merge=True, if False this loops just copies dictionaries and applies subtract_time to avoid overwrites later
    for j,ph in enumerate(dict_of_phases) :
        if j>0 : # we already have last point of first phase, or of previous phase
            if all_merged :
                if smoothed_dict_to_use is not None:
                    off_from_last_phase = last_point - smoothed_dict_to_use[ph][1][0]
                else :
                    off_from_last_phase = last_point - dict_of_phases[ph][1][0] # always first point as this could be very steep
        # apply offset
        aligned_phases[ph] = [
            dict_of_phases[ph][0].copy() - subtract_time , # subtract_time is zero unless association_start_time_to_zero is True
            dict_of_phases[ph][1].copy() + off_from_last_phase , # off_from_last_phase is 0 unless all_merged is True
        ]
        if smoothed_dict_to_use is not None:
            aligned_smoothed_phases[ph] = [
                smoothed_dict_to_use[ph][0].copy() - subtract_time ,
                smoothed_dict_to_use[ph][1].copy() + off_from_last_phase ,
            ]
        # get last nm value of this phase, after the correction! (useful only if all_merge=True)
        if smoothed_dict_to_use is not None:
            last_point = aligned_smoothed_phases[ph][1][-1] # last point
        else :        
            last_point = numpy.mean(aligned_phases[ph][1][-3:]) # use mean of last 3 points
    if all_merged and print_k!=False :
        print(' align_phase_dict(%s) all_merged=True off_from_last_phase= %g [smoothed_dict_to_use=%s]'%(print_k,off_from_last_phase,smoothed_dict_to_use is not None))
    # now align_to_begin_of_phase (which has been set to association if association_begins_at_zero)
    off=0
    if align_to_begin_of_phase is not None :
        if smoothed_dict_to_use is not None :
            off = aligned_smoothed_phases[align_to_begin_of_phase][1][0]
        else :
            off = aligned_phases[align_to_begin_of_phase][1][0]
        if print_k!=False :
            print(' align_phase_dict(%s) Aligning at begin of phase %s with offset %g [smoothed_dict_to_use=%s]'%(print_k,align_to_begin_of_phase,off,smoothed_dict_to_use is not None))
    else : # align using mean of baseline
        LL =len(aligned_phases[align_to_phase][1])
        if smoothed_dict_to_use is not None :
            off = numpy.mean(aligned_smoothed_phases[align_to_phase][1][2*LL//3:])
        else :
            off = numpy.mean(aligned_phases[align_to_phase][1][2*LL//3:]) 
        if print_k!=False :
            print(' align_phase_dict(%s) Aligning to mean of last third of baseline (%d datapoints) with offset %g [smoothed_dict_to_use=%s]'%(print_k,len(aligned_phases['baseline'][1][2*LL//3:]),off,smoothed_dict_to_use is not None))
    # subtract off
    for ph in aligned_phases :
        aligned_phases[ph][1] -= off
    return aligned_phases


def get_plot_range_from_multi_sensor_phase_dict(all_phase_dict, dilate_y_range_by_fraction=0.05,dilate_x_range_by_fraction=0):
    '''
    return y_range, x_range
    '''
    mins_x,maxs_x,mins_y,maxs_y=[],[],[],[]
    for sensor in all_phase_dict :
        for phase in all_phase_dict[sensor] :
            mins_x+=[numpy.nanmin(all_phase_dict[sensor][phase][0])]
            mins_y+=[numpy.nanmin(all_phase_dict[sensor][phase][1])]
            maxs_x+=[numpy.nanmax(all_phase_dict[sensor][phase][0])]
            maxs_y+=[numpy.nanmax(all_phase_dict[sensor][phase][1])]
    m,M=min(mins_y),max(maxs_y)
    y_range = (m - dilate_y_range_by_fraction * (M - m), M + dilate_y_range_by_fraction * (M - m))
    m,M=min(mins_x),max(maxs_x)
    x_range = (m - dilate_x_range_by_fraction * (M - m), M + dilate_x_range_by_fraction * (M - m))    
    return y_range, x_range

'''
OLD CODE NOT SO CLEAR:
    oldph = None
    
    
    
    
    
    for ph in dict_of_phases:
        if oldph is None:
            aligned_phases[ph] = [
                dict_of_phases[ph][0].copy(),
                dict_of_phases[ph][1].copy(),
            ]
            if smoothed_dict_to_use is not None:
                aligned_smoothed_phases[ph] = [
                    smoothed_dict_to_use[ph][0].copy(),
                    smoothed_dict_to_use[ph][1].copy(),
                ]
            oldph = ph
            continue
        # do this to make 100% sure it's a copy!
        if all_merged:
            if smoothed_dict_to_use is not None:
                # you could also JUST MERGE WITH MEDIAN OF FOUR POINTS!!!
                off = (
                    smoothed_dict_to_use[ph][1][0]
                    - aligned_smoothed_phases[oldph][1][-1]
                )
                oth = dict_of_phases[ph][1][0] - aligned_phases[oldph][1][-1]
                print(
                    "DEB: off=%g with no smooth woudl be %g percentdiff %g %s-%s"
                    % (off, oth, 100.0 * (off - oth) / oth, oldph, ph)
                )
            else:
                off = dict_of_phases[ph][1][0] - aligned_phases[oldph][1][-1]
        else:
            off = 0  # just copy the dictionary in this case

        aligned_phases[ph] = [
            dict_of_phases[ph][0].copy(),
            dict_of_phases[ph][1].copy() - off,
        ]
        if smoothed_dict_to_use is not None:
            aligned_smoothed_phases[ph] = [
                smoothed_dict_to_use[ph][0].copy(),
                smoothed_dict_to_use[ph][1].copy() - off,
            ]
        oldph = ph
    if association_begins_at_zero:  # adjust y-values
        if align_to_begin_of_phase not in dict_of_phases:
            print(
                "**POTENTIAL ERROR in align_phase_dict input dict_of_phases does not have '%s' has key - not doing association_begins_at_zero"
                % (align_to_begin_of_phase)
            )
            return aligned_phases
        if smoothed_dict_to_use is not None:
            off = aligned_smoothed_phases[align_to_begin_of_phase][1][0]
        else:
            off = aligned_phases[align_to_begin_of_phase][1][0]
        for ph in aligned_phases:
            aligned_phases[ph][1] -= off
    else : # align so that mean of baseline is zero, if baseline present
        if 'baseline' in dict_of_phases :
            LL =len(aligned_phases['baseline'][1])
            if smoothed_dict_to_use is not None:
                off = numpy.mean(aligned_smoothed_phases['baseline'][1][LL//2:])
            else:
                off = numpy.mean(aligned_phases['baseline'][1][LL//2:])
            for ph in aligned_phases:
                aligned_phases[ph][1] -= off
        else : 
            print("**POTENTIAL WARNING** in align_phase_dict association_begins_at_zero set to False, but baseline not in dict_of_phases")
    if association_start_time_to_zero:  # adjust times
        off = aligned_phases[align_to_begin_of_phase][0][0]
        for ph in aligned_phases:
            aligned_phases[ph][0] -= off
    return aligned_phases
'''

def align_subtracted_dict(
    dict_of_phases,
    association_begins_at_zero=True,
    dissociation_to_end_of_association=True,
    end_of_baseline_to_association=True,
):
    """
    align consecutive phases and sets beginning of association to zero (unless different options are given)
    """
    aligned_phases = dict_of_phases.copy()
    if association_begins_at_zero:
        if len(aligned_phases["association"][1].shape) > 1:
            off = aligned_phases["association"][1][:, 0]
        else:
            off = aligned_phases["association"][1][0]
        aligned_phases["association"] = [
            aligned_phases["association"][0].copy(),
            aligned_phases["association"][1].copy() - off,
        ]
    if dissociation_to_end_of_association:
        if len(aligned_phases["association"][1].shape) > 1:
            off = (
                aligned_phases["dissociation"][1][:, 0]
                - aligned_phases["association"][1][:, -1]
            )
        else:
            off = (
                aligned_phases["dissociation"][1][0]
                - aligned_phases["association"][1][-1]
            )
        aligned_phases["dissociation"] = [
            aligned_phases["dissociation"][0].copy(),
            aligned_phases["dissociation"][1].copy() - off,
        ]
    if end_of_baseline_to_association:
        if len(aligned_phases["association"][1].shape) > 1:
            off = (
                aligned_phases["baseline"][1][:, -1]
                - aligned_phases["association"][1][:, 0]
            )
        else:
            off = (
                aligned_phases["baseline"][1][-1] - aligned_phases["association"][1][0]
            )
        aligned_phases["baseline"] = [
            aligned_phases["baseline"][0].copy(),
            aligned_phases["baseline"][1].copy() - off,
        ]
    return aligned_phases


def auto_assing_references_from_sensorname(sensor_names, cand_ref_names=['reference','ref'] ) :
    '''
    sensor_names can be for example the key of a dictionary
    with user-given name like "WT", "REF","ASYN" or similar
     cand_ref_names are possible keyword that may be contained in reference sensor_names in order of priority
    '''
    sensor_name_to_its_reference_name={}
    available_references=[]
    # first identify putative reference sensors
    for jc,refkw in enumerate(cand_ref_names) :
        for nam in sensor_names :
            if refkw in nam.lower() :
                available_references+=[nam]
        if len(available_references) > 0 : # identify at higher priority, don't look for others
            break
    if len(available_references)==0 :
        sys.stderr.write("**ERROR** in auto_assing_references_from_sensorname() no candidate reference sensors found by using keywords %s into sensor_names=%s"%(cand_ref_names,[s.lower() for s in sensor_names]))
        return sensor_name_to_its_reference_name, available_references
    elif len(available_references)==1 :
        print("ONLY 1 candidate reference sensor found = %s\n assigning this one to all sensors"%(available_references[0]))
        for sens in sensor_names:
            sensor_name_to_its_reference_name[sens]=available_references[0]
        return sensor_name_to_its_reference_name, available_references
    
    for nam in sensor_names :
        if nam in available_references :
            sensor_name_to_its_reference_name[nam]=nam # assign this reference to istelf
        else :
            found=False
            for cref in available_references :
                if nam.lower().strip() in cref.lower() :
                    if found :
                        sys.stderr.write("**ERROR** in auto_assing_references_from_sensorname() overwriting previously assigned reference for sensor %s (prev ref=%s; new ref=%s)"%(nam,sensor_name_to_its_reference_name[nam],cref))
                    sensor_name_to_its_reference_name[nam]=cref
                    found=True
            if not found :
                sys.stderr.write("**WARNING** in auto_assing_references_from_sensorname() cannot auto locate reference for sensor %s"%(nam))
    return sensor_name_to_its_reference_name,available_references


def subtract_references(
    PhaseDict_aligned,
    Buffer_PhaseDict_aligned,
    NonSpecific_PhaseDict_aligned=None,
    Buffer_NonSpecific_PhaseDict_aligned=None,
):
    """
    subtract reference sensor (typically probing buffer, and can also give None if want to subracts only non-specific binding) 
        and if not None subtracts also non-specific binding sensor.
    """
    # subtract references
    Sub_ref = OrderedDict()
    for ph in PhaseDict_aligned:
        Sub_ref[ph] = [
            PhaseDict_aligned[ph][0].copy(),
            PhaseDict_aligned[ph][1].copy(),
        ]  # -(P1vsTryp10[ph][1]-P1vsBuffer_aligned[ph][1])
        if Buffer_PhaseDict_aligned is not None:
            Sub_ref[ph][1] -= Buffer_PhaseDict_aligned[ph][1]
        if NonSpecific_PhaseDict_aligned is not None:
            if Buffer_NonSpecific_PhaseDict_aligned is not None:
                correction = (
                    NonSpecific_PhaseDict_aligned[ph][1]
                    - Buffer_NonSpecific_PhaseDict_aligned[ph][1]
                )
            else:
                correction = NonSpecific_PhaseDict_aligned[ph][1]
            Sub_ref[ph][1] -= correction
    return Sub_ref


def align_by_estrapolating_baseline(
    dict_of_phases,
    fit=(lambda x, p: p[0] * x + p[1], [0.1, 0.1]),
    plot=True,
    remove_factor=3,
    **kwargs
):
    """ 
    extrapolates the baseline linearly or with custom function and subtract the extrapolation 
    from all phases - fit is tuple with fit_function and parameter guess
    return aligned_phases,bas_fit_res
    use dissociation function to fit baseline:
        fit=(pd.dissociation_simple_nonfull,[1,1] )
    NOTE baseline is fitted to a range - may need amending for dissociation
    """
    t = dict_of_phases["baseline"][0]
    ys = dict_of_phases["baseline"][1]
    rem = int(len(ys) / 30)
    bas_fit_res = {}
    maxT = max([max(dict_of_phases[phase][0]) for phase in dict_of_phases])
    fitting_function_fixed_params["dissociation_simple_Y0"] = numpy.mean(
        ys[remove_factor * rem : remove_factor * rem + 1]
    )  # set this parameter in case we fit to dissociation rate, otherwise it has no effect
    f1 = plotter.profile(
        ys[remove_factor * rem : -1],
        t[remove_factor * rem : -1],
        label="baseline-extrapolated (fit)",
        linfit=fit,
        fit_linecolor=True,
        color="blue",
        fit_results=bas_fit_res,
        show=False,
        calculate_fit_CI=1000,
        plot_fit_CI=True,
        fit_allow_extra_fraction=(min(t[remove_factor * rem : -1]), maxT),
    )
    plotter.print_fit_results(bas_fit_res)
    aligned_phases = OrderedDict()
    new_start = None
    for phase in dict_of_phases:
        # if new_start is None and phase=='baseline': fitting_function_fixed_params['dissociation_simple_Y0']=dict_of_phases[phase][1][0] # not a good idea as beginning of basline may be skewed
        if new_start is not None:
            fitting_function_fixed_params[
                "dissociation_simple_Y0"
            ] = new_start  # just in case it was fit with this functino
        aligned_phases[phase] = [
            dict_of_phases[phase][0].copy(),
            dict_of_phases[phase][1].copy()
            - fit[0](dict_of_phases[phase][0], bas_fit_res["parameters_fitted"]),
        ]
        print(
            (
                phase,
                dict_of_phases[phase][0][0],
                dict_of_phases[phase][0][-1],
                "->",
                aligned_phases[phase][0][0],
                aligned_phases[phase][0][-1],
                "|",
                dict_of_phases[phase][1][0],
                dict_of_phases[phase][1][-1],
                "->",
                aligned_phases[phase][1][0],
                aligned_phases[phase][1][-1],
            )
        )
        print(
            (
                "fit",
                fit[0](dict_of_phases[phase][0], bas_fit_res["parameters_fitted"])[0],
                fit[0](dict_of_phases[phase][0], bas_fit_res["parameters_fitted"])[-1],
            )
        )
        new_start = fit[0](dict_of_phases[phase][0], bas_fit_res["parameters_fitted"])[
            -1
        ]
        print(
            (
                "fitting_function_fixed_params['dissociation_simple_Y0']",
                fitting_function_fixed_params["dissociation_simple_Y0"],
                "new_start",
                new_start,
            )
        )
    if plot:
        f1 = plot_phase_dict(
            dict_of_phases,
            label="non-subtracted",
            fit_extrapolate_baseline=False,
            figure=f1,
        )
        f1 = plot_phase_dict(
            aligned_phases,
            label="subtracted",
            fit_extrapolate_baseline=False,
            figure=f1,
            **kwargs
        )
    return aligned_phases, bas_fit_res


def _lin_equ(point1, point2):
    """Line encoded as l=(x,y)."""
    m = float(point2[1] - point1[1]) / (point2[0] - point1[0])
    c = point2[1] - (m * point2[0])
    return m, c


def align_by_dilating_baseline(
    dict_of_phases_to_align,
    reference_dict_of_phases,
    smooth=False,
    plot=True,
    add_last_dissociation_points=False,
    shift_by=100,
    **kwargs
):
    # shift_by is necessary as decimal power of negative numbers are not defined (e.g. X^1.2 is not defined for X<0)
    # if plot>=2 return aligned_phases,yref,ys,baseline_adj,tref,pfit_leastsq
    # else return aligned_phases
    t, tref = (
        dict_of_phases_to_align["baseline"][0].copy(),
        reference_dict_of_phases["baseline"][0].copy(),
    )
    ys, yref = (
        dict_of_phases_to_align["baseline"][1].copy(),
        reference_dict_of_phases["baseline"][1].copy(),
    )
    if smooth != False:
        if type(smooth) is tuple:
            wl, pol = smooth
        else:
            wl, pol = 111, 2
        if len(yref) < 1.3 * wl:
            wl = int(len(yref) // 2) + 1
        yref = misc.smooth_profile(yref, use_savgol_filter=(wl, pol))
        ys = misc.smooth_profile(ys, use_savgol_filter=(wl, pol))
    if add_last_dissociation_points:
        if type(add_last_dissociation_points) is int:
            ju = add_last_dissociation_points
        else:
            ju = 10
        t = numpy.append(t, dict_of_phases_to_align["dissociation"][0][-ju:])
        tref = numpy.append(tref, reference_dict_of_phases["dissociation"][0][-ju:])
        ys = numpy.append(ys, dict_of_phases_to_align["dissociation"][1][-ju:])
        yref = numpy.append(yref, reference_dict_of_phases["dissociation"][1][-ju:])
    if len(tref) != len(t) or any(t != tref):
        sys.stderr.write(
            "**ERROR** align_by_dilating_baseline different times! len(tref)=%d len(t)=%d any(t!=tref)\n"
            % (len(tref), len(t))
        )
    # make positive
    tref -= tref.min()
    t -= t.min()
    yref += shift_by
    ys += shift_by
    ref_0 = yref[0]
    y_0 = ys[0]
    fun = (
        lambda y_to_align, p: ref_0 * (y_to_align / y_0) ** p[0]
    )  # assumes baselines are exponential decays.. so fitting parameter is an exponent
    pfit_leastsq, perr_leastsq, s_sq, sum_of_squared_residuals = misc.fit_function(
        fun, ys, yref, p_guess=[1]
    )
    corr = None
    if (
        not pfit_leastsq[0] > 0.5 and pfit_leastsq[0] < 2
    ):  # method not suitable, (IDEALLY SHOULD BE CLOSE TO 1)
        m, q = _lin_equ((0, shift_by), (len(ys) - 1, shift_by - 0.05))
        corr = lambda x: x * m + q
        print(
            "WARNING pfit_leastsq[0]=%g but to use shoud be close to 1"
            % (pfit_leastsq[0])
        )
        ys += corr(numpy.arange(len(ys)))
        yref += corr(numpy.arange(len(yref)))
        ref_0 = yref[0]  # redefine zeros
        y_0 = ys[0]
        fun = (
            lambda y_to_align, p: ref_0 * (y_to_align / y_0) ** p[0]
        )  # assumes baselines are exponential decays.. so fitting parameter is an exponent
        pfit_leastsq, perr_leastsq, s_sq, sum_of_squared_residuals = misc.fit_function(
            fun, ys, yref, p_guess=[1]
        )
    baseline_adj = fun(ys, pfit_leastsq)
    print(
        "Fit of baseline, R,pval=",
        scipy.stats.pearsonr(baseline_adj, yref),
        "pfit_leastsq=",
        pfit_leastsq,
        "ys.min",
        ys.min(),
        "yref.min",
        yref.min(),
    )
    aligned_phases = OrderedDict()
    nl = 0
    for phase in dict_of_phases_to_align:
        yv = dict_of_phases_to_align[phase][1].copy()
        if corr is not None:
            aligned_phases[phase] = [
                dict_of_phases_to_align[phase][0].copy(),
                fun(yv + corr(numpy.arange(nl, len(yv) + nl)) + shift_by, pfit_leastsq)
                - shift_by
                - corr(numpy.arange(nl, len(yv) + nl)),
            ]
            nl += len(yv)
        else:
            aligned_phases[phase] = [
                dict_of_phases_to_align[phase][0].copy(),
                fun(yv + shift_by, pfit_leastsq) - shift_by,
            ]
    if plot:
        f1 = plot_phase_dicts_together(
            [reference_dict_of_phases, dict_of_phases_to_align, aligned_phases],
            labels=["Ref.", "To Align", "Aligned"],
            **kwargs
        )
    if plot >= 2:
        plotter.profile(
            [yref, baseline_adj],
            [ys, ys],
            ls=["", "-"],
            marker=[".", ""],
            markerfacecolor="none",
            label=["Input", "Fit"],
        )
        plotter.profile(
            [yref, ys, baseline_adj],
            tref,
            label=["Ref. baseline", "To align basline", "Aligned baseline"],
            title="Control baseline plot",
            xlabel="Time",
            ylabel="Binding (nm)",
            alpha=0.6,
        )
        return aligned_phases, yref, ys, baseline_adj, tref, pfit_leastsq
    return aligned_phases


def shift_to_same_times(dict_of_phases_to_shift, dict_of_phases_with_ref_time):
    """
    # shift to same times in order to plot together superimposed even when they are subsequent phases (e.g. non-specific binding and actual binding)
    """
    dict_of_phases_shifted = {}  # dict_of_phases_to_shift.copy()
    for k in dict_of_phases_to_shift:
        if k not in dict_of_phases_shifted:
            dict_of_phases_shifted[k] = [None, None]
        if len(dict_of_phases_with_ref_time[k][0]) < len(dict_of_phases_to_shift[k][0]):
            print(
                (
                    "**potential ERROR** in shift_to_same_times() len(dict_of_phases_with_ref_time[k][0]) < len(dict_of_phases_to_shift[k][0]) %d %d (length of times)"
                    % (
                        len(dict_of_phases_with_ref_time[k][0]),
                        len(dict_of_phases_to_shift[k][0]),
                    )
                )
            )
        dict_of_phases_shifted[k][0] = numpy.append(
            dict_of_phases_with_ref_time[k][0].copy()[
                : dict_of_phases_to_shift[k][1].shape[-1]
            ],
            numpy.nan,
        )
        if len(dict_of_phases_to_shift[k][1].shape) > 1:
            dict_of_phases_shifted[k][1] = numpy.append(
                dict_of_phases_to_shift[k][1].copy()[
                    :, dict_of_phases_with_ref_time[k][0].shape[-1]
                ],
                [numpy.nan] * dict_of_phases_to_shift[k][1].shape[0],
                axis=1,
            )
        else:
            dict_of_phases_shifted[k][1] = numpy.append(
                dict_of_phases_to_shift[k][1].copy()[
                    : dict_of_phases_with_ref_time[k][0].shape[-1]
                ],
                numpy.nan,
            )
    return dict_of_phases_shifted


def plot_phase_dict(
    dict_of_phases,
    fit=False,
    use_partial_dissociation=False,
    analyte_conc=None,
    xlabel="Time (s)",
    ylabel="Binding (nm)",
    label=None,
    fit_extrapolate_baseline=False,
    figure=None,
    save=None,
    fit_results={},
    calculate_fit_CI=False,
    color=None,
    parameters_name=None,
    fit_linecolor="black",
    fit_linestyle="-",
    fit_linewidth=None,
    zorder=0,
    figure_size=None,
    return_data=False,
    fit_association_only=False,
    **kwargs
):
    """
    return figure
    if return_data
    return figure,times,yvals,phase_boundaries,label
    if fit return figure,fit_data
    if fit is true it tries by default to fit association and dissociation phases (not globally)
    fit can also be a tuple with a function and a guess , e.g. 
        fit=(pd.both_fits, [40,1e-2,0.2, 0],  [[0,None],[0,None],[0,None] ]   )
              parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax' ]
     or fit=( pd.both_fits_nonfull_dissociation , [40,1e-2,dict_of_phases['dissociation'][1][0], 0 ], [ [0,None],[0,None],[0,None], [None,dict_of_phases['dissociation'][1][0]] ] ) 
              parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax','dissociation y[t->inf]' ]
     or fit=( pd.both_fits_independent , [40,1e-2,dict_of_phases['dissociation'][1][0],1e-2 ], [ [0,None],[0,None],[0,None], [0,None] ] ) 
              parameters_name=['kass (1/Ms)','kdiss(Ass.phase) (1/s)','Rmax','kdiss(Diss.phase) (1/s)']
    fit_association_only is useful if you are fitting a loading phase and want to do quantitation.
    """
    vlines = []
    yvals = None
    xtimes = None
    fit_tuple = None
    if type(fit) is tuple:
        fit_tuple = fit
        fit = True
    bas_fit_res = {}
    #if isinstance(dict_of_phases, OrderedDict):
    iter_on = list(dict_of_phases.keys())
    #else:
    #    iter_on = ["baseline", "association", "dissociation"]
    key_association=None # only used  if fitting
    key_dissociation=None
    for phase in iter_on:
        if phase in dict_of_phases:
            if key_association is None and "association" in phase.lower() :
                key_association=phase
            if key_dissociation is None and "dissociation" in phase.lower() :
                key_dissociation=phase
            t = dict_of_phases[phase][0]
            ys = dict_of_phases[phase][1]
            if fit_extrapolate_baseline and phase == "baseline":
                rem = int(len(ys) / 10)
                plotter.profile(
                    ys[rem:-rem],
                    t[rem:-rem],
                    linfit=True,
                    fit_results=bas_fit_res,
                    show=False,
                )
            if t.shape[-1] != ys.shape[-1]:
                print(
                    (
                        "**ERROR** in plot_phase_dict given dict_of_phases with differnt numbers of times %s and data values %s for '%s' - double check subtractions!!"
                        % (str(t.shape), str(ys.shape), phase)
                    )
                )
                return
            if xtimes is None:
                xtimes = t
            else:
                xtimes = numpy.hstack((xtimes, t))
            if yvals is None:
                yvals = ys
            else:
                yvals = numpy.hstack((yvals, ys))
            vlines += [t[0]]
        else:
            print(
                ("Potential WARNING plot_phase_dict '%s' not in dict_of_phases" % (phase))
            )
    if fit_extrapolate_baseline and bas_fit_res != {}:
        # draw the line corresponding to the baseline fit
        figure = plotter.profile(
            bas_fit_res["pol_fun"](xtimes),
            xtimes,
            linewidth=0.8,
            alpha=0.7,
            vgrid=False,
            figure=figure,
            zorder=zorder - 1,
            figure_size=figure_size,
        )
    if fit:
        if fit_tuple is None:
            fit_tup = (
                both_fits,
                [400, 1e-2, dict_of_phases[key_dissociation][1][0]],
                [[0, None], [0, None], [0, None]],
            )
            if use_partial_dissociation:
                fit_tup = (
                    both_fits_nonfull_dissociation,
                    [400, 1e-2, dict_of_phases[key_dissociation][1][0], 0],
                    [
                        [0, None],
                        [0, None],
                        [0, None],
                        [None, dict_of_phases[key_dissociation][1][0]],
                    ],
                )
            # fit_tup = ( both_fits_independent , [40,1e-2,dict_of_phases['dissociation'][1][0],1e-2 ], [ [0,None],[0,None],[0,None], [0,None] ] )
            custom_tuple=False
        else:
            fit_tup = fit_tuple
            custom_tuple=True
        if analyte_conc is None:
            print(
                "**WARNING** [IGNORE if you are doing quantitation] analyte_conc is None in fit - should be given as input (protein concentration in the association rate)"
            )
            analyte_conc = 1
        else:
            active_fraction = 1
            analyte_conc = active_fraction * analyte_conc * params["ConvertToMolar"]
        if parameters_name is None:
            parameters_name = [
                "kass (1/Ms)",
                "kdiss (1/s)",
                "Rmax",
                "dissociation y[t->inf]",
            ]  # likely guess
        if fit_tup[0] == both_fits_independent:
            parameters_name = [
                "kass (1/Ms)",
                "kdiss[Association] (1/s)",
                "Rmax",
                "kdiss[Dissociation] (1/s)",
            ]
        if not custom_tuple :
            fitting_function_fixed_params["both_fits_analyte_conc"] = analyte_conc
            fitting_function_fixed_params["both_fits_t_break"] = dict_of_phases[key_dissociation][0][0]
            print("FITTING with analyte_conc=%s M and Tbreak= %g s expected Rmax=%g"% (
                    str(fitting_function_fixed_params["both_fits_analyte_conc"]),
                    fitting_function_fixed_params["both_fits_t_break"],
                    dict_of_phases[key_dissociation][1][0],))
        
        # print ('vlines',vlines)
        # plot just fit with no marker
        if type(fit_linecolor) is bool and fit_linecolor == True:
            fit_linecolor = color
        if fit_association_only :
            figure = plotter.profile(dict_of_phases[key_association][1], dict_of_phases[key_association][0],
                fit_linecolor=fit_linecolor,
                zorder=zorder + 1,
                ls="",
                marker=",",
                markeredgecolor="none",
                vgrid=False,
                markerfacecolor="none",
                calculate_fit_CI=calculate_fit_CI,
                plot_fit_CI=True,
                linfit=fit_tup,
                fit_results=fit_results,
                fit_allow_extra_fraction=False,
                fit_linestyle=fit_linestyle,
                fit_linewidth=fit_linewidth,
                figure_size=figure_size,
                figure=figure,
            )
        else :
            figure = plotter.profile(
                numpy.hstack(
                    (dict_of_phases[key_association][1], dict_of_phases[key_dissociation][1])
                ),
                numpy.hstack(
                    (dict_of_phases[key_association][0], dict_of_phases[key_dissociation][0])
                ),
                fit_linecolor=fit_linecolor,
                zorder=zorder + 1,
                ls="",
                marker=",",
                markeredgecolor="none",
                vgrid=False,
                markerfacecolor="none",
                calculate_fit_CI=calculate_fit_CI,
                plot_fit_CI=True,
                linfit=fit_tup,
                fit_results=fit_results,
                fit_allow_extra_fraction=False,
                fit_linestyle=fit_linestyle,
                fit_linewidth=fit_linewidth,
                figure_size=figure_size,
                figure=figure,
            )
        # plot full bit including other times (e.g. baseline)
        extra_str = ""
        figure = plotter.profile(
            yvals,
            xtimes,
            label=label,
            vgrid=vlines,
            xlabel=xlabel,
            ylabel=ylabel,
            figure=figure,
            save=save,
            zorder=zorder,
            color=color,
            **kwargs
        )   
        extra_str = label #kwargs["label"]
        fit_data = plotter.print_fit_results(
            fit_results,
            out=sys.stdout,
            parameters_name=parameters_name,
            return_data_class=True,
            extra_str=extra_str,
        )
        if not fit_association_only :
            Kd, Kd_err = misc.propagate_error(
                lambda x, y: x / y,
                [fit_results["parameters_fitted"][1], fit_results["parameters_fitted"][0]],
                [
                    fit_results["parameters_standard_errors"][1],
                    fit_results["parameters_standard_errors"][0],
                ],
            )
            sys.stdout.write(
                "Kd= %g +/- %g M\n***********************************************************\n\n"
                % (Kd, Kd_err)
            )
            fit_data["Kd (M)"] = [Kd, Kd_err]
        if save is not None:
            fit_data.Print(save.split(".")[0] + extra_str + "_fitResults.txt")
        if return_data :
            return figure,xtimes,yvals,vlines,label
        return figure   ,fit_data
    else:
        figure = plotter.profile(
            yvals,
            xtimes,
            label=label,
            vgrid=vlines,
            xlabel=xlabel,
            ylabel=ylabel,
            figure=figure,
            save=save,
            zorder=zorder,
            color=color,
            figure_size=figure_size,
            **kwargs
        )
    if return_data :
        return figure,xtimes,yvals,vlines,label
    return figure


def plot_phase_dicts_together(
    list_of_dict_of_phases,
    labels=None,
    color=None,
    xlabel="Time (s)",
    ylabel="Binding (nm)",
    save=None,
    fit=False,
    global_fit=False,
    analyte_concentrations=None,
    use_partial_dissociation=False,
    parameters_name=None,
    fit_linecolor=None,
    fit_linewidth=None,
    figure=None,
    **kwargs
):
    """
    
    fit can also be a tuple with a function and a guess , e.g. 
        fit=(pd.both_fits, [40,1e-2,0.2, 0],  [[0,None],[0,None],[0,None] ]   )
              parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax' ]
     or fit=( pd.both_fits_nonfull_dissociation , [40,1e-2,dict_of_phases['dissociation'][1][0], 0 ], [ [0,None],[0,None],[0,None], [None,dict_of_phases['dissociation'][1][0]] ] ) 
              parameters_name=['kass (1/Ms)','kdiss (1/s)','Rmax','dissociation y[t->inf]' ]
     or fit=( pd.both_fits_independent , [40,1e-2,dict_of_phases['dissociation'][1][0],1e-2 ], [ [0,None],[0,None],[0,None], [0,None] ] ) 
              parameters_name=['kass (1/Ms)','kdiss(Ass.phase) (1/s)','Rmax','kdiss(Diss.phase) (1/s)']
    """
    if labels is None:
        labels = [None] * len(list_of_dict_of_phases)
    if color is None:
        color = [None] * len(list_of_dict_of_phases)
    # first check times and their length, if needed shift some
    longest = OrderedDict()
    for ph in list_of_dict_of_phases[0]:
        reftimes = None
        for j, dict_of_phases in enumerate(list_of_dict_of_phases):
            if reftimes is None:
                reftimes = dict_of_phases[ph][0]
            elif len(reftimes) != len(dict_of_phases[ph][0]):
                if ph not in longest:
                    longest[ph] = [0, len(reftimes)]  # reftime is the 0
                if longest[ph][1] < len(dict_of_phases[ph][0]):
                    longest[ph] = [j, len(dict_of_phases[ph][0])]
    if len(longest) == 0:  # no shift needed
        shifted_phases = list_of_dict_of_phases
    else:  # shift
        l = misc.uniq([longest[k][0] for k in longest])
        if len(l) > 1:
            print(
                (
                    "**Potential ERROR** in plot_phase_dicts_together phase_dicts have different times with different number of points - but one must have more points in all phases size_dict=%s"
                    % (str(longest))
                )
            )
        l = l[0]
        print(("size_dict", longest, l))
        shifted_phases = [
            shift_to_same_times(df, list_of_dict_of_phases[l].copy())
            if j != l
            else list_of_dict_of_phases[l].copy()
            for j, df in enumerate(list_of_dict_of_phases)
        ]
    # check for fit and plot
    fit_tuple = None
    fit_linestyle = "-"
    if global_fit is not None and not (
        type(global_fit) is bool and global_fit == False
    ):
        ys_glob_fit, xs_glob_fit, tbreaks = [], [], []
        if type(global_fit) is tuple:
            fit_tuple = global_fit
        if analyte_concentrations is None:
            print(
                "\n**ERROR** GIVEN global_fit but kwarg analyte_concentrations is None\n"
            )
        elif type(fit) is tuple:
            fit_tuple = fit  # save it
        # if fit : print("WARN fit=True and global_fit given, setting fit to False to do only global.")
        fit = True  # Do individual fit and try to use for better parameter guess for global
        fit_linestyle = ""  # don't plot individual fit
    if analyte_concentrations is None:
        analyte_concentrations = [None] * len(shifted_phases)
    if len(analyte_concentrations) != len(shifted_phases):
        print(
            "\n**ERORR** len(analyte_concentrations) != len(shifted_phases) %d and %d\n"
            % (len(analyte_concentrations), len(shifted_phases))
        )
    fig = figure
    local_fit_results = []
    hline = None
    if fit:
        hline = 0
    if "hline" in kwargs:
        hline = kwargs["hline"]
    # plot (and fit locally if global_fit is true so to get a good starting guess)
    for j, dict_of_phases in enumerate(shifted_phases):
        fr = {}
        print("labels[j]", labels[j])
        fig = plot_phase_dict(
            dict_of_phases,
            hline=hline,
            label=labels[j],
            figure=fig,
            color=color[j],
            fit=fit,
            analyte_conc=analyte_concentrations[j],
            fit_results=fr,
            fit_linestyle=fit_linestyle,
            fit_linewidth=fit_linewidth,
            fit_linecolor=fit_linecolor,
            xlabel=xlabel,
            ylabel=ylabel,
            parameters_name=parameters_name,
            save=None,
            use_partial_dissociation=use_partial_dissociation,
            **kwargs
        )
        if global_fit is not None and not (
            type(global_fit) is bool and global_fit == False
        ):
            local_fit_results += [fr["parameters_fitted"]]
            ys_glob_fit += [
                numpy.hstack(
                    (
                        dict_of_phases["association"][1],
                        dict_of_phases["dissociation"][1],
                    )
                )
            ]
            xs_glob_fit += [
                numpy.hstack(
                    (
                        dict_of_phases["association"][0],
                        dict_of_phases["dissociation"][0],
                    )
                )
            ]
            tbreaks += [dict_of_phases["association"][0][-1]]
    # Fit
    if global_fit is not None and not (
        type(global_fit) is bool and global_fit == False
    ):
        local_fit_results = numpy.array(local_fit_results)
        # print ('local_fit_results=',local_fit_results,local_fit_results.shape)
        # print ('numpy.median(numpy.array(local_fit_results),axis=0)=',numpy.median(numpy.array(local_fit_results),axis=0))
        if fit_tuple is None:
            fit_tup = (
                both_fits,
                list(numpy.median(local_fit_results, axis=0))[:2]
                + [
                    dict_of_phases["dissociation"][1][0]
                    for dict_of_phases in shifted_phases
                ],
            )
            if use_partial_dissociation:
                fit_tup = (
                    both_fits_nonfull_dissociation,
                    list(numpy.median(local_fit_results, axis=0))[:2]
                    + misc.flatten(
                        [
                            [dict_of_phases["dissociation"][1][0], 0.02]
                            for dict_of_phases in shifted_phases
                        ]
                    ),
                )
            # fit_tup = ( both_fits_independent , [40,1e-2,dict_of_phases['dissociation'][1][0],1e-2 ], [ [0,None],[0,None],[0,None], [0,None] ] )
        else:
            fit_tup = fit_tuple
        if parameters_name is None:
            parameters_name = (
                ["kass (1/Ms)", "kdiss (1/s)"]
                + ["Rmax " + str(c) for c in analyte_concentrations]
                + ["dissociation y[t->inf]"]
            )  # likely guess, first 3 are always right
            if fit_tup[0] == both_fits_independent:
                parameters_name = (
                    ["kass (1/Ms)", "kdiss[Association] (1/s)"]
                    + ["Rmax " + str(c) for c in analyte_concentrations]
                    + ["kdiss[Dissociation] (1/s)"]
                )

        local_params_indices = list(range(2, len(fit_tup[1])))

        # analyte_conc=None,Tbreak=None
        analyte_concentrations = [
            a * params["ConvertToMolar"] for a in analyte_concentrations
        ]
        local_args_for_global = misc.flatten(zip(analyte_concentrations, tbreaks))
        ax = fig.gca()
        xm, xM = ax.get_xlim()
        ym, yM = ax.get_ylim()
        fit_results = {}
        print("\nGLOBAL FIT:")
        print(
            "DEB fit_tup",
            fit_tup,
            "local_args_for_global (analyte Conc, Tbreak)=",
            local_args_for_global,
            "len(xs_glob_fit)",
            len(xs_glob_fit),
            "len(ys_glob_fit)",
            len(ys_glob_fit),
            "analyte_concentrations=",
            analyte_concentrations,
        )
        # plotter.profile(ys_glob_fit,xs_glob_fit, linfit=fit_tup,local_params_indices=local_params_indices , local_args_for_global=local_args_for_global, fit_global=True,fit_results=fit_results,fit_allow_extra_fraction=False)
        # print ('xs_glob_fit=',xs_glob_fit,numpy.array(xs_glob_fit).min())
        # print ('ys_glob_fit=',ys_glob_fit,numpy.array(ys_glob_fit).min())
        ax = plotter.add_fit(
            ax,
            xs_glob_fit,
            ys_glob_fit,
            fit_tup,
            local_params_indices=local_params_indices,
            local_args_for_global=local_args_for_global,
            linewidth=fit_linewidth,
            linecolor=fit_linecolor,
            alpha=None,
            linestyle="-",
            fit_allow_extra_fraction=False,
            label=None,
            fit_results=fit_results,
        )
        ax.set_xlim(xm, xM)
        ax.set_ylim(ym, yM)
        plotter.plt.draw()
        extra_str = "Global Fit"
        fit_data = plotter.print_fit_results(
            fit_results,
            out=sys.stdout,
            parameters_name=parameters_name,
            return_data_class=True,
            extra_str=extra_str,
        )
        Kd, Kd_err = misc.propagate_error(
            lambda x, y: x / y,
            [fit_results["parameters_fitted"][1], fit_results["parameters_fitted"][0]],
            [
                fit_results["parameters_standard_errors"][1],
                fit_results["parameters_standard_errors"][0],
            ],
        )
        sys.stdout.write(
            "Kd= %g +/- %g M\n***********************************************************\n\n"
            % (Kd, Kd_err)
        )
        fit_data["Kd (M)"] = [Kd, Kd_err]
        if save is not None:
            fit_data.Print(save.split(".")[0] + extra_str + "_fitResults.txt")
    if save is not None:
        if hasattr(fig, "__len__"):
            fig[0].savefig(save, transparent=True)
        else:
            fig.savefig(save, transparent=True)
    return fig


def plot_raw_data(
    smoothed_raws,
    assay_step,
    value_labels=None,
    columns_to_plot=None,
    sensor_names_dict=None,
    color=plotter.palette20,
    y_range=(0, None),
    legend_size=12,
    phase_rot="vertical",
    save=None,
    **kwargs
):
    """
    smoothed_raws can also be just raw
    """
    phase_beginnings = assay_step.column("Step Begins")
    f = None
    if columns_to_plot is None:
        columns_to_plot = smoothed_raws.HD()
    if sensor_names_dict is None:
        label = columns_to_plot
    else:
        for l in columns_to_plot :
            if ' ' in l and ' '.join(l.split()[1:]) not in sensor_names_dict :
                sys.stderr.write("%s not in sensor_names given as input, setting to itself\n"% (repr(' '.join(l.split()[1:]))))
                sensor_names_dict[' '.join(l.split()[1:])]=' '.join(l.split()[1:])
        if any(["smoothed" in c.lower() for c in columns_to_plot]):
            label = [
                "Smoothed " + sensor_names_dict[' '.join(l.split()[1:])] # remove "smoothed " or "Smoothed " at beginning of key (this would be 'Smoothed A1' but sometimes A1 can be replaced with things containing spaces)
                if "smoothed" in l.lower()
                else l
                for l in columns_to_plot
            ]
        else:
            label = [sensor_names_dict[l] for l in columns_to_plot]
    #print("if you get a warning about an empty string '' not in sensor_names it's typically safe to ignore. You probably have extra empty columns in your time-interpolated data file.")
    f = smoothed_raws.profile(
        columns_to_plot,
        "keys",
        label=label,
        figure=f,
        color=color,
        y_range=y_range,
        vline=phase_beginnings,
        legend_size=legend_size,
        **kwargs
    )
    if value_labels is not None:
        ax = f.gca()
        m, ymax = ax.get_ylim()
        for xt in value_labels:
            _ = ax.annotate(
                value_labels[xt],
                (xt, ymax),
                (0, -20),
                xycoords="data",
                size=8,
                textcoords="offset points",
                rotation=phase_rot,
                ha="center",
                va="top",
            )
        plotter.plt.draw()
        plotter.plt.show(block=False)
    if save is not None:
        f.savefig(save, transparent=True)
    return f


def read_octet_log(logfile="OctetExperiment.log"):
    date_time_data = csv_dict.Data()
    read_octet_log.hd = csv_dict.list_to_dictionary(["TimeSTR", "time", "message"])
    nstep = 0
    step_data = csv_dict.Data()
    step_data.hd = csv_dict.list_to_dictionary(
        [
            "start_time",
            "start message",
            "start location",
            "end_time",
            "timedelta_from_start",
            "duration (s)",
            "seconds from start",
            "end message",
            "end status",
            "end location",
        ]
    )
    step_data.key_column_hd_name = "Step #"
    # datetime(year, month, day[, hour[, minute[, second)
    for j, line in enumerate(open(logfile)):
        dt, message = line.replace("\n", "").replace("\r", "").split("\t")
        time = datetime.datetime.strptime(dt, "%Y/%m/%d %H:%M:%S")
        date_time_data[j] = [dt, time, message]
        if (
            message[: len("Processing sample")] == "Processing sample"
        ):  # maybe 'Waiting to start sample' instead
            if nstep in step_data:
                print("**ERROR** nstep in step_data")
            location = message.split("location")[1].strip()
            step_data[nstep] = [time, message, location]
            if len(step_data) == 1:
                start = time  # fist is start of experiment
        elif message[: len("Sample completed")] == "Sample completed":
            location = message.split("location")[1].strip()
            delta = time - step_data[nstep][step_data.hd["start_time"]]
            step_data[nstep] += [
                time,
                time - start,
                delta.seconds,
                (time - start).seconds,
                message,
                "completed",
                location,
            ]
            nstep += 1
        elif message[: len("Assay step skipped")] == "Assay step skipped":
            # location=message.split('location')[1].strip() HOPEFULLY will be set by beginning
            delta = time - step_data[nstep][step_data.hd["start_time"]]
            step_data[nstep] += [
                time,
                time - start,
                delta.seconds,
                (time - start).seconds,
                message,
                "skipped",
                location,
            ]
            nstep += 1
    step_data._equate_to_same_length()
    return step_data, date_time_data


def read_octet_tracer(tracerfile="Tracer.log"):
    date_time_data = csv_dict.Data()
    read_octet_log.hd = csv_dict.list_to_dictionary(["TimeSTR", "time", "message"])
    nstep = 0
    step_data = csv_dict.Data()
    step_data.hd = csv_dict.list_to_dictionary(
        [
            "start_time",
            "start message",
            "XYZdelay",
            "total_delay",
            "end_time",
            "timedelta_from_start",
            "duration (s)",
            "seconds from start",
            "end message",
            "RunTime",
            "AccumulatedRunTime",
            "XYZdelaySTA",
            "total_delaySTA",
        ]
    )
    step_data.key_column_hd_name = "Step #"
    with open(tracerfile) as fff:
        lines = fff.read().splitlines()
    last_RunTime, XYZdelay, total_delay = 0, 0, 0
    # datetime(year, month, day[, hour[, minute[, second)
    for j, line in enumerate(lines):
        try:
            line = line.replace("\n", "").replace("\r", "")
            dt, message = line[:21].strip(), line[22:].strip()
            # if message not in ['START SCAN:','STOP SCAN:'] : continue
            time = datetime.datetime.strptime(
                dt + "00000", "%Y/%m/%d %H:%M:%S.%f"
            )  # read also the microsecond
            date_time_data[j] = [dt, time, message]
            if (
                message[: len("START SCAN")] == "START SCAN"
            ):  # maybe 'Waiting to start sample' instead
                if nstep in step_data:
                    print("**ERROR** nstep in step_data")
                step_data[nstep] = [
                    time,
                    message,
                    XYZdelay / 1000.0,
                    total_delay / 1000.0,
                ]
                if len(step_data) == 1:
                    start = time  # fist is start of experiment
                XYZdelay, total_delay = 0, 0
            elif message[: len("STOP SCAN")] == "STOP SCAN":
                RunTime = float(lines[j + 3].split("RunTime (ms)      =")[-1])
                duration = time - step_data[nstep][step_data.hd["start_time"]]
                duration_seconds = float(
                    str(duration.seconds) + "." + str(duration.microseconds)
                )
                elapsed_time = time - start
                elapsed_seconds = float(
                    str(elapsed_time.seconds) + "." + str(elapsed_time.microseconds)
                )
                last_RunTime += RunTime
                step_data[nstep] += [
                    time,
                    elapsed_time,
                    duration_seconds,
                    elapsed_seconds,
                    message,
                    RunTime / 1000.0,
                    last_RunTime / 1000.0,
                    XYZdelay / 1000.0,
                    total_delay / 1000.0,
                ]
                XYZdelay, total_delay = 0, 0
                nstep += 1
            elif "XYZ movement time" in message:
                XYZdelay, total_delay = misc.get_numbers_from_string(message)
        except Exception:
            print("\nException at line %d: '%s'" % (j, str(line)))
            raise
    step_data._equate_to_same_length()
    return step_data, date_time_data


def read_phase_from_tracer(tracer_file):
    with open(tracer_file) as fff:
        assay_summary = (
            fff.read().split("ASSAY SUMMARY:")[-1].splitlines()
        )  # note there are more than one ASSAY SUMMARY in typical tracers
    tracer_data = csv_dict.Data()
    phase_beginning = []
    HD = None
    for line in assay_summary:
        if "====" in line or "-----" in line or len(line) < 10:
            continue
        if "Step" in line:
            if HD is not None:
                print("WARNING overwriting HD in tracer %s" % (tracer_file))
            HD = list(map(str.strip, line.split("  ")))  # two spaces..
            tracer_data.key_column_hd_name = HD[1]
            HD = HD[2:]
            tracer_data.hd = csv_dict.list_to_dictionary(HD)
        else:
            l = [
                csv_dict.convert_to_number(a)[0]
                for a in map(str.strip, line.split("  "))
                if a != ""
            ]
            if l[1] in tracer_data:
                print("ERROR overwriting %s in tracer_data" % (str(l[1])))
            tracer_data[l[1]] = l[2:]
    if "Duration" not in tracer_data.hd:
        print("ERROR Duration not in tracer_data")
    else:
        phase_beginning = [0]
        cumulative = 0
        for k in tracer_data:
            if type(k) is int or type(k) is float:  # step numbers
                cumulative += tracer_data[k][tracer_data.hd["Duration"]]
                phase_beginning += [cumulative]
                tracer_data[k] += [cumulative]
            else:
                tracer_data[k] += [""]
        tracer_data._update_hd("Time")
    tracer_data._equate_to_same_length()
    return tracer_data, phase_beginning


def split_into_phases(
    phases, raw_data, phase_begin_key="Start Time", phase_name_key="Step Name"
):
    raw_data.numpymat()
    phase_dict = csv_dict.Data()
    phase_dict.hd = csv_dict.list_to_dictionary(
        phases.HD() + ["Raw times", "Times"] + raw_data.HD()
    )
    phase_beginnings = phases.column(phase_begin_key)
    times = numpy.array(list(raw_data.keys()))
    for j, k in enumerate(phases):
        nk = phases[k][phases.hd[phase_name_key]]
        if nk in phase_dict:
            nk = csv_dict.new_key_for_dict(phase_dict, nk)
        phases[k][phases.hd[phase_name_key]] = nk
        el, ind = misc.find_nearest(times, phases[k][phases.hd[phase_begin_key]])
        if j + 1 < len(phases):
            el_end, ind_end = misc.find_nearest(times, phase_beginnings[j + 1])
            ind_end += 1
        else:
            ind_end = None
        phase_dict[nk] = (
            phases[k]
            + [times[ind:ind_end], times[ind:ind_end] - times[ind]]
            + list(raw_data.mat[ind:ind_end].T)
        )
    return phase_dict


def process_multi_phase(
    phase_data_sort,
    restart_from_zero=True,
    plot_smoothed=False,
    remove_nseconds={"baseline": 20, "association": 1, "dissociation": 1},
    smooth=True,
):
    aligned_data_sort = []
    for jp, phase_data in enumerate(phase_data_sort):
        if restart_from_zero is True:
            rest_phase = restart_phases_from(
                phase_data,
                phases_to_restart_from_first=[
                    "baseline",
                    "association",
                    "dissociation",
                ],
                restart_from=0,
            )
        else:
            rest_phase = phase_data
        if remove_nseconds is not None:
            trim_phase_data = remove_first_few_seconds(
                rest_phase,
                nseconds=remove_nseconds,
                only_phases=["baseline", "association", "dissociation"],
            )
        else:
            trim_phase_data = phase_data
        # we can first smooth and align actually
        if smooth:
            smoothed_phase_data = smooth_phase_data(
                trim_phase_data,
                savgov_win=71,
                savgov_polorder=3,
                savgov_mode="interp",
                only_phases=None,
                plot_smoothed=plot_smoothed,
            )
        else:
            smoothed_phase_data = trim_phase_data
        # align baseline, association and dissociation
        als = align_phases(
            smoothed_phase_data,
            subtract_mean_end_of_baseline=50,
            force_association_start0=True,
            force_dissociation_to_association_end=True,
            smooth_first=False,
        )
        aligned_data_sort += [als]
        # subtract reference sample and - if asked - sensors
        # sub_data_sample = correct_references(phase_data,sensor_layout, input_sensor_samplRef_pairs=[['A', 'F'],['B', 'F'],['C', 'G'],['D', 'G'],['E', 'H']] ,subtract_reference_sensor=True,subtract_reference_sample=True,smooth_first=True,savgov_win=121,savgov_polorder=3,savgov_mode='mirror')
    return aligned_data_sort


def select_phases_from_plot(data):
    f = None
    ys = [data.column(k) for k in data.HD()]
    x = list(map(float, list(data.keys())))
    f = plotter.profile(
        ys,
        x,
        label=[a[0] for a in data.HD()],
        figure=f,
        ls="",
        marker=".",
        markersize=8,
        markerfacecolor=True,
        markeredgecolor=True,
        color=plotter.plt.get_cmap("jet"),
        vline=None,
        save=None,
        show=False,
        xlabel="close window after saving all phase-begin points",
    )
    ax = f.gca()
    xs = numpy.array(x * len(ys)).flatten()
    ys = numpy.array(ys).flatten()
    curs = plotter.SaveCursor(
        ax,
        xs,
        ys,
        add_vline=True,
        first_message="Select points (one per sensor) corresponding to phase beginnings",
    )
    print(
        "\nClose figure %d to continue after selecting points that separate phases"
        % (f.number)
    )
    plotter.plt.show()  # don't put block
    print(sorted(curs.saved))
    return curs.saved


def name_phases_manually(data, phase_begin):
    f = None
    phase_beginnings = phase_begin[:]
    if (
        (0 in data or "0" in data or 0.0 in data)
        and 0 not in phase_beginnings
        and 0.0 not in phase_beginnings
    ):
        phase_beginnings += [0.0]
    phase_beginnings = misc.uniq(phase_beginnings)
    phase_beginnings.sort()
    f = data.profile(
        data.HD(),
        "keys",
        label=[a[0] for a in data.HD()],
        figure=f,
        ls="",
        marker=".",
        markersize=8,
        markerfacecolor=True,
        markeredgecolor=True,
        color=plotter.plt.get_cmap("jet"),
        vline=phase_beginnings,
    )
    print("look at figure to name phases")
    shortcuts = {
        "b": "baseline",
        "a": "association",
        "d": "dissociation",
        "e": "exclude",
        "r": "regeneration",
    }
    shortstr = "available shortcuts:\n" + "\n".join(
        ["%s = %s" % (k, shortcuts[k]) for k in sorted(shortcuts)]
    )
    ax = f.gca()
    my = sum(ax.get_ylim()) / 2.0
    stx, endx = ax.get_xlim()
    names = []
    for j in range(len(phase_beginnings)):
        if j >= len(phase_beginnings) - 1:
            b, e = phase_beginnings[j], endx
        else:
            b, e = phase_beginnings[j], phase_beginnings[j + 1]
        a = ax.annotate(
            "name this part",
            xy=((b + e) / 2.0, my),
            ha="center",  # xycoords='figure fraction',
            va="center",
            bbox=dict(boxstyle="round,pad=0.5", fc="gray", alpha=0.5),
            # arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0')
        )
        a.set_visible(True)
        plotter.plt.draw()
        n = input("name of phase in grey? t in (%lf %lf)?\n%s\n" % (b, e, shortstr))
        if n.lower() in shortcuts:
            name = shortcuts[n.lower()]
        else:
            name = n
        print("NAME: ", name)
        a.set_visible(False)
        names += [name]
    return names, phase_beginnings


def get_phase_manually(data):
    phase_beginnings = select_phases_from_plot(data)
    phase_beginnings = [p[0] for p in phase_beginnings]
    phases, phase_beginnings = name_phases_manually(data, phase_beginnings)
    return phase_beginnings, phases


def exclude_concentrations(dict_of_phases_HD_concentrations, exclude=[85.5]):
    if not hasattr(exclude, "__len__"):
        exclude = [exclude]
    new_dict = OrderedDict()
    for ph in dict_of_phases_HD_concentrations:
        new_dict[ph] = csv_dict.Data()
        hd = [c for c in dict_of_phases_HD_concentrations[ph].HD() if c not in exclude]
        print("phase:", ph, "concentrations left:", hd)
        for t in dict_of_phases_HD_concentrations[ph]:
            new_dict[ph][t] = []
            for c in hd:
                new_dict[ph][t] += [
                    dict_of_phases_HD_concentrations[ph][t][
                        dict_of_phases_HD_concentrations[ph].hd[c]
                    ]
                ]
        new_dict[ph].hd = csv_dict.list_to_dictionary(hd)
    return new_dict


def plot_phases(
    data1,
    data2=None,
    phases=["baseline", "association", "dissociation"],
    cols1=plotter.plt.get_cmap("jet"),
    cols2=plotter.plt.get_cmap("autumn"),
    figure_size=None,
    title=None,
    figure=None,
    x_range=None,
    alpha1=None,
    alpha2=None,
    save=None,
    label=True,
    **kwargs
):
    f = figure
    tmin, tmax = 9e9, -1
    titl = ""
    v_lines = []
    for ph in phases:
        if tmin > list(data1[ph].keys())[0]:
            tmin = list(data1[ph].keys())[0]
        if tmax < list(data1[ph].keys())[-1]:
            tmax = list(data1[ph].keys())[-1]
        titl += ph + " "
    tmin -= 0.04
    tmax += 0.04
    for phase in phases[:-1]:
        if data2 is not None:
            f = data2[phase].profile(
                data2[phase].HD(),
                "keys",
                figure=f,
                label=False,
                plot_cbar_if_given=False,
                color=cols2,
                alpha=alpha2,
                figure_size=figure_size,
                **kwargs
            )
            v_lines += [list(data2[phase].keys())[-1]]
        f = data1[phase].profile(
            data1[phase].HD(),
            "keys",
            figure=f,
            label=False,
            color=cols1,
            plot_cbar_if_given=False,
            x_range=None,
            figure_size=figure_size,
            alpha=alpha1,
            **kwargs
        )
        v_lines += [list(data1[phase].keys())[-1]]
    phase = phases[-1]
    if title is None:
        title = titl
    if x_range is None:
        x_range = (tmin, tmax)
    if data2 is not None:
        f = data2[phase].profile(
            data2[phase].HD(),
            "keys",
            figure=f,
            plot_cbar_if_given=False,
            color=cols2,
            alpha=alpha2,
            **kwargs
        )
    if type(label) is bool and label is True:
        labs = data1[phase].HD()
    else:
        labs = label
    print("labels=", labs)
    f = data1[phase].profile(
        data1[phase].HD(),
        "keys",
        figure=f,
        color=cols1,
        x_range=x_range,
        plot_cbar_if_given=False,
        label=list(map(str, labs)),
        title=title,
        alpha=alpha1,
        save=save,
        vline=v_lines,
        **kwargs
    )
    return f


def read_rawdata_file(
    fname,
    key_column_hd_name="Time (s)",
    expected_reads_every_second=0.2,
    thershold_signal_phase_sep=None,
    debug=True,
    delimiter=' '
):
    """
    read data from all sensors
    expected_reads_every_second is the inverse of the frequency in Hz and can be used to guess the time at which different phases (e.g. association-dissociation) are
    performend, in fact for big enough frequencies (small expected_reads_every_second) a delay is observed when moving the tips to the next phases
    """
    data = csv_dict.Data()
    data.filename = fname
    data.key_column_hd_name = key_column_hd_name
    with open(fname) as fff:
        lines = fff.read().splitlines()
    if "," in lines[0] :
        HD = [ h for h in lines[0].strip(",").split(",") if h not in ['',' '] ]
    else :
        HD = lines[0].split()
    guessed_phase_beginnings = []
    oldt = None
    oldvalues = numpy.zeros((len(HD)))
    oldvalues2 = oldvalues
    for j, l in enumerate(lines[1:]):
        try:
            if len(l) < 2:
                continue
            if not l[0].isdigit():
                print("skipping row:", l)
                continue
            if "," in l:
                entries = list(map(float, l.strip().strip(",").split(",")))
            else:
                entries = list(map(float, l.split()))
            times = entries[::2]  # all times, these are repeated for each sensor and should all be the same (checked after)
            values = entries[1::2]  # all values
            if len(values) != len(HD):
                print(
                    "ERROR in read_rawdata_file %s len(values)!=len(HD) %d %d at line %d"
                    % (fname, len(values), len(HD), j + 2)
                )  # +2 as 0 is HD not in loop
            t = misc.uniq(times)
            if len(t) > 1:
                print(
                    "ERROR in read_rawdata_file %s at line %d non unique times!! found %s saving all under %lf"
                    % (fname, j + 2, str(t), t[0])
                )
            data[t[0]] = values
            if (
                len(guessed_phase_beginnings) == 0
                or t[0] - guessed_phase_beginnings[-1] > 3
            ):  # >3 cares for oldvalues and oldvalues2
                if oldt is None:
                    guessed_phase_beginnings += [t[0]]
                elif (
                    expected_reads_every_second is not None
                    and t[0] - oldt > 1.3 * expected_reads_every_second
                ):
                    if debug:
                        print(" DEBttim at t=", t[0], "tdiff=", t[0] - oldt)
                    guessed_phase_beginnings += [t[0]]
                    # below we consider the half-sensors with biggest change and do mean - half with biggest should remove baselines and cases where change is not expected, means outliers outside the noise range.
                elif (
                    thershold_signal_phase_sep is not None
                    and (
                        numpy.sort((numpy.abs(oldvalues2 - numpy.array(values))))[
                            len(oldvalues) / 2 :
                        ]
                    ).mean()
                    > thershold_signal_phase_sep
                ):
                    if debug:
                        print(
                            " DEBdiff at t=",
                            t[0],
                            "v=",
                            (
                                numpy.sort(
                                    (numpy.abs(oldvalues2 - numpy.array(values)))
                                )[len(oldvalues) / 2 :]
                            ).mean(),
                            "diff_vals=",
                            numpy.sort((numpy.abs(oldvalues2 - numpy.array(values)))),
                        )
                    guessed_phase_beginnings += [oldt]
                    # guessed_phase_beginnings+=[t[0]]
            if debug and 2969.44 < t[0] < 2971.25:
                print(
                    " *DEBcus at t=",
                    t[0],
                    "v=",
                    ( numpy.sort((numpy.abs(oldvalues2 - numpy.array(values))))[ len(oldvalues) // 2 ]).mean(),
                    "diff_vals=",
                    numpy.sort((numpy.abs(oldvalues2 - numpy.array(values)))),
                )
            oldt = t[0]
            oldvalues2 = oldvalues
            oldvalues = numpy.array(values)
        except Exception:
            print(("RAISED at line:", j))
            print(l)
            raise
    data.hd = csv_dict.list_to_dictionary(HD)
    if expected_reads_every_second is not None:
        print(
            "guessed %d phases beginning at seconds: %s"
            % (len(guessed_phase_beginnings), str(guessed_phase_beginnings))
        )
    return data, guessed_phase_beginnings


def read_plate_layout_and_phases(fname, delimiter=",", guessed_phase_beginnings=None):
    phases = None
    phase_beginning = None
    with open(fname) as fff:
        lines = fff.read().splitlines()
    sensor_layout = (
        {}
    )  # keys are row of the plate ('A','B','C',..) and values are ['sensor type',concentration]
    for l in lines:
        if len(l) < 4 or l[0] == "#":
            continue
        entries = list(map(str.strip, l.split(delimiter)))
        if len(entries) == 0:
            continue
        if phases is None:
            phases = entries[:]
        elif l[:16] == "phase_beginning=":
            phase_beginning = list(map(str.strip, l[16:].split(delimiter)))
            phase_beginning = sorted(map(float, phase_beginning))
            if (
                guessed_phase_beginnings is not None
                and guessed_phase_beginnings != phase_beginning
            ):
                print(
                    "WARNING guessed_phase_beginnings!=phase_beginning:\n\t%s\n\t%s"
                    % (str(guessed_phase_beginnings), str(phase_beginning))
                )
        else:
            k, t, c = entries
            c = float(c)
            if k in sensor_layout:
                print(
                    "ERROR in read_plate_layout_and_phases() k %s in sensor_layout overwriting"
                    % (k)
                )
            sensor_layout[k] = [t, c]
    if phases is None:
        print("ERROR phases not found must be in first non commented (#) row of file!!")
    else:
        phases = list(map(str.lower, phases))
        if "association" not in phases:
            print(
                "ERROR association not in phases, baseline,association,dissociation are compulsory phases!"
            )
        if "dissociation" not in phases:
            print(
                "ERROR dissociation not in phases, baseline,association,dissociation are compulsory phases!"
            )
        if "baseline" not in phases:
            print(
                "ERROR baseline not in phases, baseline,association,dissociation are compulsory phases!"
            )
    if phase_beginning is None and guessed_phase_beginnings is not None:
        phase_beginning = guessed_phase_beginnings
    return phases, sensor_layout, phase_beginning


def get_interesting_phases_multiple(
    data,
    phase_beginnings,
    phases,
    interesting_phases=["baseline", "association", "dissociation"],
):
    phase_data = {}
    phase_data_sort = []
    n = 0
    phase_data_sort += [OrderedDict()]
    beginnings = []
    for j, f in enumerate(phases):
        # check if dictionary is full
        full = True
        for fa in interesting_phases:
            if fa not in phase_data_sort[n]:
                full = False
        if full:  # add new
            phase_data_sort += [OrderedDict()]
            n += 1
        if f in interesting_phases:  # update
            bg = phase_beginnings[j]
            print("Phase %s beginning at %lf" % (f, bg), end=" ")
            if j < len(phases) - 1:
                end = phase_beginnings[j + 1]
                print(" until %lf" % (end))
                dat = data.group_by_condition(None, lambda x: x >= bg and x < end)
            else:
                print(" last phase")
                dat = data.group_by_condition(None, lambda x: x >= bg)
            phase_data_sort[n][f] = dat
    return phase_data_sort


def get_interesting_phases(
    data,
    phase_beginnings,
    phases,
    interesting_phases=["baseline", "association", "dissociation"],
):
    phase_data = {}
    beginnings = []
    for f in interesting_phases:
        if f not in phases:
            print(
                "\nERROR %s interesting phase not in input phases! %s"
                % (f, str(phases))
            )
        j = phases.index(f)
        bg = phase_beginnings[j]
        beginnings += [bg]
        print("Phase %s beginning at %lf" % (f, bg), end=" ")
        if j + 1 == len(phase_beginnings):
            print(" last phase")
            dat = data.group_by_condition(None, lambda x: x >= bg)
        else:
            end = phase_beginnings[j + 1]
            print(" until %lf" % (end))
            dat = data.group_by_condition(None, lambda x: x >= bg and x < end)
        phase_data[f] = dat
        # could consider smoothing
        # sm=misc.smooth_profile(data.column(3), use_savgol_filter=(19,3))
        # d1,dtimes= misc.derivative(data.column(0),data.keys())
    phase_data_sort = OrderedDict()
    beginnings, interesting_phases = list(
        zip(*sorted(zip(beginnings, interesting_phases)))
    )
    for f in interesting_phases:  # now sorted
        phase_data_sort[f] = phase_data[f]
        del phase_data[f]
    return phase_data_sort


def restart_phases_from(
    phase_data,
    phases_to_restart_from_first=["baseline", "association", "dissociation"],
    restart_from=0,
):
    # if phases_to_restart_from_first is more than one phase the first of the list will be restarted from the input time "restart_from"
    # and the others will be offsetted in the same way so that they remain consecutive.
    if type(phases_to_restart_from_first) is str:
        phases_to_restart_from_first = [phases_to_restart_from_first]
    start_time = list(phase_data[phases_to_restart_from_first[0]].keys())[0]
    offset = start_time - restart_from
    restarted_phase_data = OrderedDict()
    for phase in phases_to_restart_from_first:
        restarted_phase_data[phase] = phase_data[phase].copy(
            deep=False
        )  # does not copy items
        for t in phase_data[phase]:
            restarted_phase_data[phase][t - offset] = phase_data[phase][t][:]
    return restarted_phase_data


def remove_first_few_seconds(
    phase_data, nseconds=1, only_phases=["baseline", "association", "dissociation"]
):
    if type(nseconds) is not dict and hasattr(nsecodns, "__len__"):
        ns = {}
        if len(nseconds) == len(phase_data) and isinstance(phase_data, OrderedDict):
            for j, phase in enumerate(phase_data):
                ns[phase] = nseconds[j]
        elif only_phases is not None:
            for j, phase in enumerate(only_phases):
                ns[phase] = nseconds[j]
        else:
            raise Exception(
                "\nERROR in remove_first_few_seconds nseconds should be dict or same len as phase_data if only_phases is None"
            )
        nsecodns = ns
    elif type(nseconds) is not dict:
        ns = {}
        if only_phases is not None:
            for j, phase in enumerate(only_phases):
                ns[phase] = nseconds
        else:
            for j, phase in enumerate(phase_data):
                ns[phase] = nseconds
        nseconds = ns
    sh_phase_data = OrderedDict()
    # do a new phase_data with all starting ns later according to each phase
    for phase in phase_data:
        if only_phases is not None and phase not in only_phases:
            continue  # don't smooth phases we are not interested in - don't even save them
        sh_phase_data[phase] = phase_data[phase].copy(
            deep=True
        )  # so that the input class is not actually smoothed (hopefully as it copies the csv_dict Data class..)
        ts = list(sh_phase_data[phase].keys())[0]
        te = ts
        while te - ts <= nseconds[phase]:
            del sh_phase_data[phase][te]
            te = list(sh_phase_data[phase].keys())[0]
    return sh_phase_data


def remove_phases_and_rejoin(phase_dict, phases_to_remove=[],rejoin_times=True,rejoin_ys=True, print_k='',plot=False,label=None,figure=None, **plot_phase_dict_kwargs) :
    '''
    deletes one or more phases and re-joins both times and y-value alignment.
    useful for example in some single sensor multi-concentration scans when too high concentrations (at plateua) are scanned
    then one can remove these association/dissociation (s) and re-glue with the final long
    dissociation for optimal fitting
    '''
    modified_phase_dict = {} # python3
    phases_to_rejoin = []
    all_phases =[]
    phases_removed = []
    for jp,phase in enumerate(phase_dict) :
        if phase not in phases_to_remove :
            if phases_to_rejoin!=[] and len(phases_to_rejoin[-1])==1 :
                phases_to_rejoin[-1] += [ (phase,phase_dict[phase][0][0], phase_dict[phase][1][0]) ] # add as last phase to re-join
            modified_phase_dict[phase] = [ phase_dict[phase][0].copy(), phase_dict[phase][1].copy() ]
        else :
            phases_removed+=[phase]
            if phases_to_rejoin==[] or len(phases_to_rejoin[-1]) > 1 : # add new breakage 
                if all_phases==[] : # we are removing first ever phase, so not much to re-join with previous saved one
                    phases_to_rejoin+=[ [(None, phase_dict[phase][0][0], phase_dict[phase][1][0]),] ]
                else : # we have a previous phase we will need to re-join
                    phases_to_rejoin+=[ [(all_phases[-1], phase_dict[all_phases[-1]][0][-1], phase_dict[all_phases[-1]][1][-1]),] ] # add ass initial phase to re-join
        all_phases += [ phase ]
    # check
    if len([ph for ph in phases_to_remove if ph not in phases_removed])>0 :
        print("\n**WARNING** in remove_phases_and_rejoin(%s) asked to remove %s phases but removed only %s\n"%(print_k,phases_to_remove,phases_removed))
    else :
        print(" in remove_phases_and_rejoin(%s) phases_removed=%s"%(print_k,phases_removed))
    time_offset = 0 
    y_offset = 0
    ngap=0
    in_gap=False
    for jp,phase in enumerate(phase_dict) :   
        if phase not in modified_phase_dict :
            if not in_gap :
                if rejoin_times :
                    delta_time = phases_to_rejoin[ngap][0][1] - phases_to_rejoin[ngap][1][1]
                    print (' remove_phases_and_rejoin(%s)-> gap %d re-joining %s with %s using delta_time=%g'%(print_k,ngap,str(phases_to_rejoin[ngap][0][0]),str(phases_to_rejoin[ngap][1][0]),delta_time))
                    time_offset += delta_time
                if rejoin_ys :
                    delta_ys = phases_to_rejoin[ngap][0][2] - phases_to_rejoin[ngap][1][2]
                    print (' remove_phases_and_rejoin(%s)-> gap %d re-joining %s with %s using delta_ys = %g'%(print_k,ngap,str(phases_to_rejoin[ngap][0][0]),str(phases_to_rejoin[ngap][1][0]),delta_ys))
                    y_offset += delta_ys
            in_gap=True
        else :
            if in_gap : 
                ngap += 1
            modified_phase_dict[phase][0] += time_offset
            modified_phase_dict[phase][1] += y_offset
            in_gap=False
    if plot :
        if label is None :
            label=print_k
        figure = plot_phase_dict(modified_phase_dict , label=label+' removed',figure=figure)
        figure = plot_phase_dict(phase_dict , label=label,figure=figure, **plot_phase_dict_kwargs)
    return modified_phase_dict
                
        
        
    
def smooth_phase_data(
    phase_data,
    savgov_win=71,
    savgov_polorder=3,
    savgov_mode="interp",
    only_phases=None,
    plot_smoothed=False,
):
    sm_phase_data = OrderedDict()
    for phase in phase_data:
        if only_phases is not None and phase not in only_phases:
            continue  # don't smooth phases we are not interested in - don't even save them
        sm_phase_data[phase] = phase_data[phase].copy(
            deep=True
        )  # so that the input class is not actually smoothed (hopefully as it copies the csv_dict Data class..)
        sm_phase_data[phase].numpymat(
            include_keys=False
        )  # false as we don't smooth the times
        sm_phase_data[phase].mat = scipy.signal.savgol_filter(
            sm_phase_data[phase].mat,
            window_length=savgov_win,
            polyorder=savgov_polorder,
            deriv=0,
            mode=savgov_mode,
            delta=1.0,
            axis=0,
            cval=0.0,
        )
        sm_phase_data[phase].from_numpymat()  # put in new entries
    if plot_smoothed:
        tmin, tmax = (
            list(phase_data["baseline"].keys())[0],
            list(phase_data["dissociation"].keys())[-1],
        )
        f = None
        for phase in sm_phase_data:
            f = phase_data[phase].profile(
                phase_data[phase].HD(),
                "keys",
                figure=f,
                color=plotter.Oranges,
                plot_cbar_if_given=False,
            )
            f = sm_phase_data[phase].profile(
                sm_phase_data[phase].HD(),
                "keys",
                figure=f,
                color=plotter.Blues,
                x_range=(tmin, tmax),
                plot_cbar_if_given=False,
            )
    return sm_phase_data


def _process_ch(ch, store, phase_data, phase, op, store_to_add=None, debug=True):
    if store_to_add is not None and op is not None:
        if op == "+":
            if debug:
                print("adding store", end=" ")
            store += vals
        elif op == "-":
            if debug:
                print("subtracting store", end=" ")
            store -= vals
        op = None
        store_to_add = None
    if ch == " ":
        n += 1
        return
    elif ch in sens_hds:
        j = sens_hds.index(ch)
        vals = numpy.array(phase_data[phase].column(j))
        if op is None:
            if debug:
                print("from ", j, ch, "; ", end=" ")
            store = vals
        elif op == "+":
            if debug:
                print("adding ", j, ch, "; ", end=" ")
            store += vals
        elif op == "-":
            if debug:
                print("subtracting ", j, ch, "; ", end=" ")
            store -= vals
        op = None
    elif ch in ["+", "-"]:
        op = ch

    else:
        print("CH ", ch, " not recognised")
    n += 1
    return n, store, op, store_to_add


def correct_references_manual(
    phase_data,
    pairs_operation="(A-G)-(E-F)",
    new_key=None,
    correct_phases=["baseline", "association", "dissociation"],
):
    """
    new data class will contain sensor letters in header
    input_sensor_samplRef_pairs is a list of lists like [['A','B'],['C','D'],... ] where the first is the sample sensor and the second the reference one wishes to subtract (e.g. 'A'-'B' and 'C'-'D')
    """
    if sensors_are_single_letters:
        sens_hds = [s[0] for s in phase_data[correct_phases[0]].HD()]
    else:
        sens_hds = [s for s in phase_data[correct_phases[0]].HD()]
    subtracted_phases = OrderedDict()
    for ph in correct_phases:
        subtracted_phases[phase] = phase_data[phase].copy(deep=True)
        op = None
        if type(pairs_operation) is str:
            new_column = numpy.zeros(len(phase_data[phase]))
            n = 0
            while n < len(pairs_operation):
                ch = pairs_operation[n]
                if ch == "(":
                    n += 1
                    store = numpy.zeros(len(phase_data[phase]))
                    op = None
                    while pairs_operation[n] != ")":
                        if n > 1000:
                            raise Exception("ERROR not found ) after (")
                            break
                        ch = pairs_operation[n]
                        n, store, op, store_to_add = _process_ch(
                            ch, store, phase_data, phase, op
                        )
                    if ch == ")":
                        n, new_column, op, store_to_add = _process_ch(
                            ch, new_column, phase_data, phase, op, store_to_add=store
                        )
                n, new_column, op, store_to_add = _process_ch(
                    ch, new_column, phase_data, phase, op, store_to_add=None
                )
        print("***ERROR*** END OF FUNCTION NOT PROPERLY CODED")
        subtracted_phases[ph] = n
    return subtracted_phases


def correct_references(
    phase_data,
    sensor_layout,
    input_sensor_samplRef_pairs=None,
    reference_sample=None,
    correct_phases=["baseline", "association", "dissociation"],
    subtract_reference_sensor=True,
    subtract_reference_sample=True,
    smooth_first=True,
    savgov_win=71,
    savgov_polorder=3,
    savgov_mode="interp",
    plot_smoothed=False,
):
    """
    new data class will contain concentration in header, subtracted for reference sample (the 0) and if asked reference sensors (the empty sensors)
    check align_phase_dict
    could make sense to correct baseline as well so that then the end of the dissociation phase can be set to the end of the baseline (at t->inf)
    subtract_reference_sensor will subtract to each sample sensor the corresponding reference sensor (the one without coating
    subtract_reference_sample will then subtract the 0 concentration
    input_sensor_samplRef_pairs is a list of lists like [['A','B'],['C','D'],... ] where the first is the sample sensor and the second the reference one wishes to subtract (e.g. 'A'-'B' and 'C'-'D')
    """
    # map columns to corresponding sensors
    column_to_sensor = {}
    sensor_to_column = {}
    for h in phase_data[correct_phases[0]].hd:
        if h in sensor_layout:
            v = h
        elif h[0] in sensor_layout:
            v = h[0]
        if v in sensor_to_column:
            print(
                "**WARNING** multiple columns pointing to sensor %s (last is %s and already found %s)"
                % (v, h, sensor_to_column[v])
            )
        column_to_sensor[h] = v
        sensor_to_column[v] = h
    # check that you found columns for all sensors in the input layout
    for k in sensor_layout:
        if k not in sensor_to_column:
            print("**WARNING** sensor %s not associated with any column" % (k))
    # determine the pairs of sensor (sample and reference) probing the same concentrations
    # below reference sample is the loaded sensor probing a 0 concentration, while referense sensors are unloaded sensors (empty) probing various concentrations
    sensor_pairs = (
        {}
    )  # keys will be concentrations and values will be pairs of sensor (sample_sensor,reference_sensor) reference will be None if no references are given
    for k in sensor_layout:
        c = sensor_layout[k][1]  # concentration value
        if c not in sensor_pairs:
            sensor_pairs[c] = [None, None]
        if sensor_layout[k][0] == "sample sensor":
            sensor_pairs[c][0] = k  # found sample sensor
        elif sensor_layout[k][0] == "reference sensor":
            sensor_pairs[c][1] = k  # found reference sensor
        else:
            print("**ERROR** SENSOR type %s not recognised" % (sensor_layout[k][0]))
    # print
    print(
        "\nPairs of sample and reference sensor identified [subtract_reference_sensor=%s subtract_reference_sample=%s]:"
        % (str(subtract_reference_sensor), str(subtract_reference_sample))
    )
    for c in sorted(sensor_pairs):
        print("  Concentration:", c, " -> ", sensor_pairs[c])
    if input_sensor_samplRef_pairs is not None:
        print("\ninput_sensor_samplRef_pairs given as input")
        inp_sensor_pairs = {}
        for c in sensor_pairs:
            for pair in input_sensor_samplRef_pairs:
                if sensor_pairs[c][0] == pair[0]:
                    inp_sensor_pairs[c] = pair
            if c not in inp_sensor_pairs:
                print(
                    "WARNING in ninput_sensor_samplRef_pairs identified sample sensor %s not found in input pairs!! Skipping!"
                    % (sensor_pairs[c][0])
                )
        sensor_pairs = inp_sensor_pairs.copy(deep=True)
        print("Now using following pairs:")
        for c in sorted(sensor_pairs):
            print("  Concentration:", c, " -> ", sensor_pairs[c])
        print("\n")
    # identify the reference sample (the 0 concentration)
    print("sensor_pairs:", sensor_pairs)
    reference_sample_reference_col = None
    if reference_sample is None:
        if 0 in sensor_pairs:
            reference_sample = sensor_pairs[0]
        elif 0.0 in sensor_pairs:
            reference_sample = sensor_pairs[0.0]
    if reference_sample is not None:
        reference_sample_col = sensor_to_column[
            reference_sample[0]
        ]  # 0 is the sample sensor, loaded.
        if len(reference_sample) > 1 and reference_sample[1] is not None:
            reference_sample_reference_col = sensor_to_column[
                reference_sample[1]
            ]  # we have a reference sensor (empty) for the reference sample (0 concentration)
    print("reference_sample", reference_sample, "\n")
    # smooth the data if required!
    if not smooth_first:
        sm_phase_data = phase_data
    else:
        sm_phase_data = smooth_phase_data(
            phase_data,
            savgov_win=savgov_win,
            savgov_polorder=savgov_polorder,
            savgov_mode=savgov_mode,
            only_phases=correct_phases,
            plot_smoothed=plot_smoothed,
        )
    # now subtract the phases
    corr_data = OrderedDict()
    for phase in sm_phase_data:
        if phase not in correct_phases:
            continue
        corr_data[phase] = csv_dict.Data()
        corr_data[phase].hd = csv_dict.list_to_dictionary(sorted(sensor_pairs.keys()))
        for c in sorted(sensor_pairs.keys()):
            sample_col = sensor_to_column[sensor_pairs[c][0]]
            if sensor_pairs[c][1] is not None:
                reference_column = sensor_to_column[sensor_pairs[c][1]]
            else:
                reference_column = None
            for t in sm_phase_data[phase]:
                v = sm_phase_data[phase][t][sm_phase_data[phase].hd[sample_col]]
                if subtract_reference_sensor and reference_column is not None:
                    v -= sm_phase_data[phase][t][
                        sm_phase_data[phase].hd[reference_column]
                    ]
                if subtract_reference_sample:
                    if reference_sample is not None:
                        if (
                            subtract_reference_sensor
                            and reference_sample_reference_col is not None
                        ):
                            v -= (
                                sm_phase_data[phase][t][
                                    sm_phase_data[phase].hd[reference_sample_col]
                                ]
                                - sm_phase_data[phase][t][
                                    sm_phase_data[phase].hd[
                                        reference_sample_reference_col
                                    ]
                                ]
                            )
                        else:
                            v -= sm_phase_data[phase][t][
                                sm_phase_data[phase].hd[reference_sample_col]
                            ]
                if t not in corr_data[phase]:
                    corr_data[phase][t] = [v]
                else:
                    corr_data[phase][t] += [v]
    return corr_data


def align_phases(
    corr_data,
    subtract_mean_end_of_baseline=50,
    force_association_start0=True,
    force_dissociation_to_association_end=True,
    smooth_first=True,
    savgov_win=71,
    savgov_polorder=3,
    plot_smoothed=False,
    flip_data=False,
):
    # subtract_mean_end_of_baseline is the number of timepoints on which the end of the baseline is calculated
    if not smooth_first:
        sm_data = corr_data
    else:
        sm_data = {}
        for phase in corr_data:
            sm_data[phase] = corr_data[phase].copy(
                deep=True
            )  # so that the input class is not actually smoothed (hopefully as it copies the dict..)
            sm_data[phase].numpymat(
                include_keys=False
            )  # false as we don't smooth the times
            sm_data[phase].mat = scipy.signal.savgol_filter(
                sm_data[phase].mat,
                window_length=savgov_win,
                polyorder=savgov_polorder,
                deriv=0,
                delta=1.0,
                axis=0,
                mode="mirror",
                cval=0.0,
            )
            sm_data[phase].from_numpymat()  # put in new entries
        if plot_smoothed:
            tmin, tmax = (
                list(corr_data["baseline"].keys())[0],
                list(corr_data["dissociation"].keys())[-1],
            )
            f = None
            for phase in corr_data:
                f = corr_data[phase].profile(
                    corr_data[phase].HD(), "keys", figure=f, color=plotter.Oranges
                )
                f = sm_data[phase].profile(
                    sm_data[phase].HD(),
                    "keys",
                    figure=f,
                    color=plotter.Blues,
                    x_range=(tmin, tmax),
                )
    conc_sub = {}
    assoc_to_zero = {}
    for c in sm_data["baseline"].hd:
        conc_sub[c] = numpy.mean(
            sm_data["baseline"].column(c)[-int(subtract_mean_end_of_baseline) :]
        )
    if force_association_start0:
        if type(force_association_start0) is int:
            n = force_association_start0
        else:
            n = 5  # timepoints for mean
        for c in sm_data["association"].hd:
            assoc_to_zero[c] = (
                numpy.mean(sm_data["association"].column(c)[:n]) - conc_sub[c]
            )
        print(assoc_to_zero)
    aligned_data = OrderedDict()
    for ph in sm_data:
        aligned_data[ph] = csv_dict.Data()
        aligned_data[ph].hd = sm_data[ph].hd.copy()
        for t in sm_data[ph]:
            aligned_data[ph][t] = []
            for c in sm_data[ph].HD():
                aligned_data[ph][t] += [sm_data[ph][t][sm_data[ph].hd[c]] - conc_sub[c]]
                if force_association_start0 and ph == "association":
                    aligned_data[ph][t][-1] -= assoc_to_zero[
                        c
                    ]  # for some reason offsetting the association but not the dissociation improves merging of the two...
    if force_dissociation_to_association_end:
        if type(force_dissociation_to_association_end) is int:
            n = force_dissociation_to_association_end
        else:
            n = 5  # timepoints for mean
        diss_offset = {}
        for c in aligned_data["association"].hd:
            diss_offset[c] = numpy.mean(
                aligned_data["association"].column(c)[-n:]
            ) - numpy.mean(aligned_data["dissociation"].column(c)[:n])
        print("diss_offset:", diss_offset)
        for t in aligned_data["dissociation"]:
            for j, c in enumerate(aligned_data["dissociation"].HD()):
                aligned_data["dissociation"][t][j] += diss_offset[c]
    if flip_data:
        for phase in aligned_data:
            for t in aligned_data[phase]:
                for j, c in enumerate(aligned_data[phase].HD()):
                    aligned_data[phase][t][j] *= -1.0
    return aligned_data


def phases_to_one_array(phase_data, only_phases=["association", "dissociation"]):
    # returns times, arrays , the latter being a dict with keys either concentrations or sensors and values all profiles joined together
    # then plotter.profile(arrays.values(),times,label=arrays.keys())
    if only_phases is None:
        only_phases = list(phase_data.keys())
    arrays = OrderedDict()
    times = []
    t_breaks = []
    for ph in only_phases:
        times += list(phase_data[ph].keys())
        for c in phase_data[ph].HD():
            if c not in arrays:
                arrays[c] = []
            arrays[c] += phase_data[ph].column(c)
        t_breaks += [times[-1]]
    times = numpy.array(times)
    for c in arrays:
        arrays[c] = numpy.array(arrays[c])
    return times, arrays, t_breaks


def fit_one_concentration(
    aligned_data,
    c,
    parameter_guess=[72, 1e-4, 0.2],
    active_fraction=1.0,
    fit_linecolor="red",
    fit_linewidth=2.0,
    do_also_non_full_dissociation=False,
    title=True,
    save=None,
    **kwargs
):
    times = list(aligned_data["dissociation"].keys())
    tbreak = times[0]
    times = list(aligned_data["association"].keys()) + times
    yvals = aligned_data["association"].column(c) + aligned_data["dissociation"].column(
        c
    )
    fitting_function_fixed_params["both_fits_t_break"] = tbreak
    fitting_function_fixed_params["both_fits_analyte_conc"] = (
        active_fraction * c * params["ConvertToMolar"]
    )  # in Molar
    fr, fr2 = {}, {}
    fig = plotter.profile(
        yvals,
        times,
        linfit=(both_fits, parameter_guess, [[0, None], [0, None], [0, None]]),
        fit_results=fr,
        fit_linecolor=fit_linecolor,
        fit_linewidth=fit_linewidth,
        **kwargs
    )
    if do_also_non_full_dissociation:
        fig = plotter.profile(
            yvals,
            times,
            linfit=(
                both_fits_nonfull_dissociation,
                parameter_guess + [0],
                [[0, None], [0, None], [0, None], [None, None]],
            ),
            ls="",
            marker=",",
            alpha=0.5,
            figure=fig,
            fit_results=fr2,
            fit_linecolor="magenta",
            fit_linewidth=fit_linewidth,
        )
        print("Dissociation does not converge to baseline:")
        plotter.print_fit_results(
            fr2,
            parameters_name=[
                " kass (1/Ms)",
                " kdiss (1/s)",
                " Rmax",
                " dissociation y[t->inf]",
            ],
        )
        print(
            "Kd= %lf M\n" % (fr2["parameters_fitted"][1] / fr2["parameters_fitted"][0])
        )
    print("Dissociation converges to baseline:")
    plotter.print_fit_results(
        fr, parameters_name=["kass (1/Ms)", "kdiss (1/s)", "Rmax"]
    )
    kass = fr["parameters_fitted"][0]
    kdiss = fr["parameters_fitted"][1]
    Kd, Kderr = misc.propagate_error(
        lambda x, y: x / y,
        [kdiss, kass],
        [fr["parameters_standard_errors"][1], fr["parameters_standard_errors"][0]],
    )
    Kdround, Kderrround = plotter.round_with_error(Kd, Kderr)
    sys.stdout.write("Kd= %g +/- %g M\n" % (Kdround, Kderrround))
    if title is not None:
        if type(title) is bool and title == True:
            title = "Kd= %g +/- %g M kass=%g kdiss=%g\n" % (
                Kdround,
                Kderrround,
                kass,
                kdiss,
            )
        fig.gca().set_title(title)
        plotter.plt.draw()
    if type(save) is str:
        fig.savefig(save, transparent=True, bbox_inches="tight")

    # print 'kon=',kass,'1/Ms'
    # print 'kdis=',kdiss,'1/s'
    # print 'Kd=',Kd,'M'
    return fig, Kd, Kderr


def fit_all_concentrations_from_mean(
    aligned_data,
    parameter_guess=[72, 1e-4, 0.2],
    active_fraction=1.0,
    cols=plotter.iworkpalette,
    **kwargs
):
    allKd, allKderr = [], []
    fig = None
    j = 0
    for c in aligned_data["association"].HD():
        if c <= 0:
            continue
        print("\nConcentration= %lf uM" % (c))
        fig, Kd, Kderr = fit_one_concentration(
            aligned_data,
            c,
            parameter_guess=parameter_guess,
            active_fraction=active_fraction,
            figure=fig,
            label="%g" % (c),
            color=cols[j],
            fit_linecolor=cols[j],
            **kwargs
        )
        j += 1
        allKd += [Kd]
        allKderr += [Kderr]
    print("Mean Kd unweighted", numpy.mean(allKd))
    return fig, allKd, allKderr


def fit_all_concentrations_global(
    aligned_data,
    parameter_guess=[72, 1e-4, 0.2],
    concentrations_to_fit=None,
    local_params_indices=[2],
    active_fraction=1.0,
    fit_results={},
    fit_linecolor="red",
    fit_linewidth=2.0,
    title=True,
    label=True,
    save=None,
    **kwargs
):
    if concentrations_to_fit is None:
        concentrations_to_fit = sorted(aligned_data["association"].HD())
    times = list(aligned_data["dissociation"].keys())
    tbreak = times[0]
    times = list(aligned_data["association"].keys()) + times
    yvals = []
    fitting_function_fixed_params["both_fits_t_break"] = tbreak
    fit_global = True
    local_args_for_global = []
    labls = []
    for c in concentrations_to_fit:
        if c <= 0:
            continue
        labls += ["%g uM" % (numpy.round(c, 1))]
        # print '\nConcentration= %lf uM' % (c)
        local_args_for_global += [ active_fraction * c * params["ConvertToMolar"] ]  # in Molar
        yvals += [
            aligned_data["association"].column(c)
            + aligned_data["dissociation"].column(c)
        ]
    yvals = numpy.array(yvals)
    # print 'yvals.shape=',yvals.shape
    if type(label) is bool and label == True:
        label = labls
    fig = plotter.profile(
        yvals,
        times,
        linfit=(both_fits, parameter_guess),
        label=label,
        fit_global=fit_global,
        local_params_indices=local_params_indices,
        local_args_for_global=local_args_for_global,
        fit_results=fit_results,
        fit_linecolor=fit_linecolor,
        fit_linewidth=fit_linewidth,
        **kwargs
    )
    plotter.print_fit_results(
        fit_results,
        parameters_name=["kass (1/Ms)", "kdiss (1/s)"]
        + ["Rmax"] * len(local_args_for_global),
    )
    kass = fit_results["parameters_fitted"][0]
    kdiss = fit_results["parameters_fitted"][1]
    Kd, Kderr = misc.propagate_error(
        lambda x, y: x / y,
        [kdiss, kass],
        [
            fit_results["parameters_standard_errors"][1],
            fit_results["parameters_standard_errors"][0],
        ],
    )
    Kdround, Kderrround = plotter.round_with_error(Kd, Kderr)
    sys.stdout.write("Kd= %g +/- %g M\n" % (Kdround, Kderrround))
    if title is not None:
        if type(title) is bool and title == True:
            title = "Kd= %g +/- %g M  kass (1/Ms)=%g kdiss (1/s)=%g" % (
                Kdround,
                Kderrround,
                kass,
                kdiss,
            )
        fig.gca().set_title(title)
        plotter.plt.draw()
    if type(save) is str:
        fig.savefig(save, transparent=True, bbox_inches="tight")
    return fig, Kd, Kderr
