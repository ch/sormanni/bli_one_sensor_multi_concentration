# BLI Single-Cycle Kinetics data preprocessing and fitting

## Description
This repository contain a notebook that can analyse Bio-layer interferometry (BLI) binding data obtained from a single sensor that probes multiple analyte concentrations, without the need to observe full dissociation in between them. 

For exmaple, binding data like in this figure (x-axis is time in seconds, y-axis binding signal in nm) for the interaction between the nanobody Nb24 (on the sensor) and human beta-2-microglobulin (as analyte in solution, at concentrations of 25, 50, 100, 200, and 400 nM):

<img src="./example_data_Nb24_vs_b2m/Nb24noSpacerB2M_Final_Fit_results.png" width="350">

## Installation

Make sure you have conda (or mini conda) up and running. Then from the terminal shell in the current folder install with:

```
conda env create -f environment.yml
conda activate bli
conda develop extraBLImodules
```
This will create a new conda environment named 'bli' with all the modules etc. needed to run the notebook (if you'd like a different environment name, before running these commands open the file environment.yml with any text editor and change the name in the first line).

## Usage

We recommend to first try to run the notebook `Analyze_BLI_brute_force_all.ipynb` with the example data provided (may need to allow pop-ups to open it). 
If that doesn't give any issue, then we suggest creating a new folder for each new experiment to be analysed, and making a copy of the notebook in such folder.
In fact some cells of the notebook will need to be edited according to the experiment being analysed (e.g., file names, sensor legend, analyte concentrations, etc.).
From a terminal shell in the relevant folder (the current one for the example data), run:

```
conda activate bli
jupyter notebook
```

then, once the browser opens, click the notebook `Analyze_BLI_brute_force_all.ipynb`.

**This notebook contains commented out lines (starting with `#`) in most cells. Read these very carefully as they will tell you what needs editing in the various cells to customise the notebook to your own data and analysis needs.**


## Exporting data from the BLI machine and preparing them for analysis

To run this notebook needs two files. One is the raw data from the experiment, and the other is a table with the legnth of each assay step (e.g., baseline, association, dissociation, etc.).
The first file is referred to as `Time interpolated data` in many version of the BLI analysis software, albeit we are aware that this software has changed substantially across versions and instrument models.

We recommend creating a subfolder, for example named `raw_data` in the experiment folder where to store the exported files described below.

From the analysis application of the computer connected to the BLI instrument, one can open the relvant run (double click don't drag).
Then, in the pre-process data panel at the top, there is the button to export raw data - this gives either raw or time-interpolated data as options. The two files are essentially the same with the exception of 3rd digit decimal on reads as the time-interpolated are interpolated to the closest millisecond. Overall, it's better to use the time-interpolated data but it should work with either.
In some different version of the analysis software, the time interpolated data are actually a sheet of the excel report file described below (this sheet should be copied/exported as a stand-alone .csv file).

The important thing is that this time interpolated data file is a csv file (or tab separated) formatted similar to the example one `example_data_Nb24_vs_b2m/Time Interpolated Data_nb24_vs_b2m.csv`. It has one column for the time, and then one column per sensor that was run. The header contains the sensor name (e.g. A1, B1, etc.). To avoid warning messages when running the analysis code, it's recommended to delete any empty column to the right or empty row at the bottom of such file.

Then, in the same tool bar there is the option `Export Excel Report` - here select the experiment summary and the plate and sensor images and details.

What one really needs from the Excel Report is the Assay Step Info: table, which contains the length of each step. Again, in some version of the BLI software the time-interpolated data described above may be a sheet of this report.

Copy in a new file, for example named `assay_steps.txt`, the Assay Step Info table (select the table in excel and paste it in the text file). This table is typically found in the first sheet of the Excel Report just below the line 'Assay Step Info:'. In some versions of the BLI analysis software this table is instead found in a sheet named 'Sensor Tray' under the line header 'Assay Table'. Please take a look at the example assay_steps.txt file and corresponding Excel_report_for_Nb24_vs_b2m.xls contained in example_data_Nb24_vs_b2m folder to get an idea of what this means.

Now you should have a raw_data folder inside your experiment folder that contains the excel report (not needed anymore but worth keeping it there, as it should also contain analyte concentration and other info that are needed later), the Time-interpolated data csv file and the assay_steps.txt file. The latter two are read by the notebook and are required inputs.

For usage see above, most importantly one needs to carefully read commented-out lines in each cell of the notebook as these will tell you how to customise the notebook for your data.
